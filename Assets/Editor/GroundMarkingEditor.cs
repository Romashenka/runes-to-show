﻿using UnityEngine;
using UnityEditor;

namespace RunesProj.Editors
{
    [CustomEditor(typeof(GroundMarking))]
    public class GroundMarkingEditor : Editor
    {
        private GroundMarking gM;

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            base.OnInspectorGUI();
            if (EditorGUI.EndChangeCheck())
            {
                ResetGroundMarks();
            }
            if (GUILayout.Button("Reset grid"))
            {
                gM.grid.ResetGrid();
            }
        }

        void SetPlanes()
        {
            if (gM.MainGroud != null)
                DestroyImmediate(gM.MainGroud.gameObject, true);
            if (gM.Roads != null)
            {
                for (int i = 0; i < gM.Roads.Length; i++)
                {
                    if (gM.Roads[i] != null)
                        DestroyImmediate(gM.Roads[i].gameObject, true);
                }
            }

            Transform mainGroud = PrefabUtility.InstantiatePrefab(gM.MainGroundPrefab, gM.transform) as Transform;

            Transform mainGroudTrfm = mainGroud.transform;
            Transform[] roads = new Transform[gM.RoadsNumber];
            for (int i = 0; i < roads.Length; i++)
            {
                roads[i] = PrefabUtility.InstantiatePrefab(gM.RoadPrefab, gM.transform) as Transform;
            }

            gM.SetNewRoadsAndGround(roads, mainGroudTrfm);
        }


        void ResetGroundMarks()
        {
            if (gM.MainGroud == null || gM.Roads == null || gM.Roads.Length != gM.RoadsNumber)
            {
                SetPlanes();
            }

            float planeScaleFactor = .1f;//plain mesh width is 10 when 1 scaled
            float roadsXCoordRight = gM.RoadWidth * gM.RoadsNumber * .5f
                               + gM.Spacing.x * (gM.RoadsNumber - 1) * .5f
                               + gM.transform.position.x;

            Vector3 mainGroundScale = gM.MainGroud.localScale;
            mainGroundScale.x = gM.MainGroundWidth * planeScaleFactor;
            gM.MainGroud.localScale = mainGroundScale;

            Transform[] roads = gM.Roads;
            for (int i = 0; i < roads.Length; i++)
            {
                Vector3 pos = roads[i].position;
                pos.x = roadsXCoordRight - gM.RoadWidth * .5f - (gM.RoadWidth + gM.Spacing.x) * i;
                roads[i].position = pos;

                Vector3 scale = roads[i].localScale;
                scale.x = gM.RoadWidth * planeScaleFactor;
                roads[i].localScale = scale;
            }

        }

        void OnEnable()
        {
            gM = target as GroundMarking;
            ResetGroundMarks();

            // if (gM.grid == null)
            // {
            //     gM.grid = new GroundGrid(3, 30, gM.RoadWidth, gM.Spacing, gM.enemyProb, gM.wallProb);
            // }
        }

        void OnDisable()
        {
            gM = null;
        }

    }
}