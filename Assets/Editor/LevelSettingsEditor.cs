﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RunesProj.Editors
{
    [CustomEditor(typeof(LevelSettings))]
    public class LevelSettingsEditor : Editor
    {
        private LevelSettings settings;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }


        void OnEnable()
        {
            settings = target as LevelSettings;
        }
    }
}