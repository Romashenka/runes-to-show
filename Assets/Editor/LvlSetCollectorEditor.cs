﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEditor;


namespace RunesProj.Editors
{
    [CustomEditor(typeof(LvlStgsCollector))]
    public class LvlSetCollectorEditor : Editor
    {
        private LvlStgsCollector collector;

        private List<LevelSettings> settingsCollection;

        GUILayoutOption[] objBoxOpts = new GUILayoutOption[]
        {
            GUILayout.ExpandWidth(true)
        };

        GUILayoutOption[] countOpts = new GUILayoutOption[]
        {
            GUILayout.Width(10f)
        };

        GUILayoutOption[] buttonsOpts = new GUILayoutOption[]
        {
            GUILayout.ExpandWidth(true)
        };


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (settingsCollection.Count == 0)
            {
                LevelSettings ls =
                  EditorGUILayout.ObjectField(null, typeof(LevelSettings), false, objBoxOpts) as LevelSettings;
                if (ls != null)
                {
                    Insert(0, ls);
                }
            }

            for (int i = 0; i < settingsCollection.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField((i + 1).ToString(), countOpts);

                LevelSettings currentLS = settingsCollection[i];
                LevelSettings recievedLS =
                  EditorGUILayout.ObjectField(currentLS, typeof(LevelSettings), false, objBoxOpts) as LevelSettings;
                if (currentLS != recievedLS && recievedLS != null)
                {
                    Undo.RecordObject(collector, "Changed " + i + "th level settings");
                    collector.SetNewSettingsPath(i, recievedLS);
                    Refresh();
                }

                if (GUILayout.Button("+", buttonsOpts))
                {
                    Insert(i + 1, currentLS);
                }

                if (GUILayout.Button("-", buttonsOpts))
                {
                    Undo.RecordObject(collector, "Deleted level as index " + i);
                    collector.Remove(i);
                    Refresh();
                }
                EditorGUILayout.EndHorizontal();
            }

            void Insert(int index, LevelSettings settings)
            {
                Undo.RecordObject(collector, "Added level \'" + settings.name + "\'");
                collector.Insert(index, settings);
                Refresh();
            }
        }


        void Refresh()
        {
            settingsCollection = collector.LoadAllSettings();
        }


        void OnEnable()
        {
            collector = target as LvlStgsCollector;
            Refresh();
            Undo.undoRedoPerformed += Refresh;
        }

        void OnDisable()
        {
            Undo.undoRedoPerformed -= Refresh;
            if (collector != null)
                EditorUtility.SetDirty(collector);
            collector = null;
        }
    }
}