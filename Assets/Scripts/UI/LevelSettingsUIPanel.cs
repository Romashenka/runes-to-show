﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace RunesProj
{
    public class LevelSettingsUIPanel : MonoBehaviour
    {
        public SettingsSlider settingsSliderPrefab;
        public Transform slidersParent;
        public ScriptableObject[] objectsWithSettings;


        void Start()
        {
            for (int i = 0; i < objectsWithSettings.Length; i++)
            {
                IEnumerable<FieldInfo> fields = GetFieldInfos(objectsWithSettings[i]);
                foreach (var field in fields)
                {
                    SettingsFieldAttribute attribute = field.GetCustomAttribute<SettingsFieldAttribute>();
                    SettingsSlider slider = Instantiate(settingsSliderPrefab, slidersParent);
                    slider.transform.SetAsFirstSibling();
                    slider.SetSlider(field, attribute, objectsWithSettings[i]);
                }
            }
        }

        IEnumerable<FieldInfo> GetFieldInfos(ScriptableObject settingsObject)
        {
            IEnumerable<FieldInfo> fieldInfos = settingsObject.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
                            .Where(field => field.IsDefined(typeof(SettingsFieldAttribute), false));

            return fieldInfos;
        }

    }
}