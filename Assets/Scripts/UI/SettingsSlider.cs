﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class SettingsSlider : MonoBehaviour
    {
        public Slider slider;

        public Text text;

        public string label;

        public float Value { get { return slider.value; } }

        public event Action valueChanged;

        private FieldInfo _field;
        private System.Object _fieldObject;

        public void SetSlider(FieldInfo field, SettingsFieldAttribute attribute, System.Object fieldObject)
        {
            _field = field;
            _fieldObject = fieldObject;
            slider.minValue = attribute.minValue;
            slider.maxValue = attribute.maxValue;
            label = attribute.name;
            slider.wholeNumbers = attribute.useWholeNumbers;
            System.Object val = field.GetValue(fieldObject);
            SetInitialSliderValue(val);
            slider.onValueChanged.AddListener(OnSliderValueChanged);
            OnSliderValueChanged(slider.value);
        }

        private void SetInitialSliderValue(System.Object value)
        {
            if (value is int)
            {
                slider.value = (int)value;
            }
            else if (value is float)
            {
                slider.value = (float)value;
            }
        }


        private void OnSliderValueChanged(float value)
        {
            text.text = label + " = " + value;
            _field.SetValue(_fieldObject, Convert.ChangeType(slider.value, _field.FieldType));
            valueChanged?.Invoke();
        }

    }
}