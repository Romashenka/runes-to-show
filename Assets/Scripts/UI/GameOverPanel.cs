﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverPanel : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Scene current = SceneManager.GetActiveScene();
            SceneManager.LoadScene(current.name, LoadSceneMode.Single);
        }
    }
}
