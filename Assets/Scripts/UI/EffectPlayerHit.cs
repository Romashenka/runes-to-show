﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class EffectPlayerHit : MonoBehaviour
    {
        public PlayerStatus playerStatus;
        public float effectTime = .5f;
        private Image image;


        private void Start()
        {
            playerStatus.numberOfLifesChanged += OnPlayerNumberOfLifesChanged;
            image = GetComponent<Image>();
        }

        private void OnPlayerNumberOfLifesChanged(int numberOfLifes)
        {
            if (numberOfLifes != 0)
            {
                WaitForSeconds wfs = new WaitForSeconds(effectTime);
                StartCoroutine(Red(wfs));
                IEnumerator Red(WaitForSeconds wait)
                {
                    image.enabled = true;
                    yield return wait;
                    image.enabled = false;
                }
            }
        }

        private void OnDestroy()
        {
            playerStatus.numberOfLifesChanged -= OnPlayerNumberOfLifesChanged;
        }


    }
}