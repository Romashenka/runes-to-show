﻿using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class LevelChoosePanel : MonoBehaviour
    {
        public ButtonOnClick lvlChooseButtonPrefab;

        void Start()
        {
            int numberOfLevels = LvlStgsCollector.GetLevelsCount();
            for (int i = 0; i < numberOfLevels; i++)
            {
                ButtonOnClick newButt = Instantiate(lvlChooseButtonPrefab, transform);
                Text text = newButt.GetComponentInChildren<Text>();
                text.text = "Lvl " + (i + 1);
                int localI = i;
                newButt.onClick += () => Game.GoToLevel(localI);
            }
        }
    }
}