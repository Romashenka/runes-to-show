﻿using System.Collections.Generic;
using UnityEngine;

namespace RunesProj.DrawingInput
{
    public struct InputData
    {
        public List<Vector2> breakPoints { get; }

        public InputData(List<Vector2> breakPoints)
        {
            this.breakPoints = breakPoints;
        }
    }
}