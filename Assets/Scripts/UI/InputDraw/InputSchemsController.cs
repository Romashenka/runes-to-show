﻿using UnityEngine;
using System;

namespace RunesProj.DrawingInput
{
    public class InputSchemsController
    {
        private IInputSchem[] schems;
        private Sign[] signs;


        public InputSchemsController()
        {
            signs = Game.levelSettings.AllValidSigns();
            int len = signs.Length;
            schems = new IInputSchem[len];
            for (int i = 0; i < len; i++)
            {
                schems[i] = signs[i].Schem;
            }
        }

        /// <summary>
        /// Returns first sign, that matches inputData. Returns null if there is no such sign. 
        /// </summary>
        public Sign CheckSchemes(InputData inputData)
        {
            int len = schems.Length;
            for (int i = 0; i < len; i++)
            {
                if (schems[i].CheckSchem(inputData))
                    return signs[i];
            }
            return null;
        }
    }
}