﻿using UnityEngine;
using System.Collections;

namespace RunesProj.DrawingInput
{
    public class TrailRendToInputDir : MonoBehaviour
    {

        public InputDir input;
        public float fadeTime = .2f;
        private Material trailRendMaterial;
        private TrailRenderer trailRend;

        public Camera renderCamera;
        float renderCameraZ;


        // Start is called before the first frame update
        void Start()
        {
            input.inputStarted += OnInputStarted;
            input.inputContinued += OnInputChanged;
            input.inputFinished += OnInputEnded;

            renderCameraZ = renderCamera.transform.position.z;

            trailRend = GetComponent<TrailRenderer>();
            trailRendMaterial = trailRend.material;
            trailRend.time = Mathf.Infinity;
            trailRendMaterial.color = Color.white;

        }


        void OnInputStarted(Vector2 pos)
        {
            SyncToScreenPos(pos);
            trailRend.Clear();
        }

        void OnInputChanged(Vector2 pos)
        {
            SyncToScreenPos(pos);
        }

        void OnInputEnded(Sign result, Vector2 pos)
        {
            if (result == null)
                trailRendMaterial.color = Color.red;
            else
                trailRendMaterial.color = Color.blue;
            
            StartCoroutine(FadeCorout(new WaitForSeconds(fadeTime)));
        }

        IEnumerator FadeCorout(WaitForSeconds time)
        {
            yield return time;
            trailRend.Clear();
            trailRendMaterial.color = Color.white;
        }

        void ResetTrlRend()
        {
            trailRend.Clear();
            trailRendMaterial.color = Color.white;
        }

        void SyncToScreenPos(Vector2 pos)
        {
            Vector3 deepPos = pos;
            deepPos.z = -renderCameraZ - .1f;
            transform.position = renderCamera.ScreenToWorldPoint(deepPos);
        }
    }
}