﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;


namespace RunesProj.DrawingInput
{
    public class InputDir : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler, IPointerExitHandler
    {
        public float fromLastBreakThreshold = 20f;
        private Vector2 lastPoint;
        private InputSchemsController schemContrlr;


        public Sign tapSign;


        public event Action<Vector2> inputStarted;
        public event Action<Vector2> inputContinued;
        ///<summary>
        ///Sign may be null
        ///</summary>
        public event Action<Sign, Vector2> inputFinished;

        private List<Vector2> breakPoints;
        private Vector2 lastDirFromBreak;

        private bool _pointerDown;

        void Start()
        {
            breakPoints = new List<Vector2>(6);
            schemContrlr = new InputSchemsController();
            // TurnOff();
            Game.fightStarted += TurnOn;
            Game.fightIsOver += TurnOff;
/*
            inputStarted += (Vector2 v) => Debug.Log("Started");
            inputContinued += (Vector2 v) => Debug.Log("Contin");
            inputFinished += (Sign s, Vector2 v) => Debug.Log("Finished");
*/
        }

        void TurnOn() { gameObject.SetActive(true); }
        void TurnOff() { gameObject.SetActive(false); Reset(); }

        public void OnPointerDown(PointerEventData eventData)
        {
            _pointerDown = true;
            breakPoints.Add(eventData.position);
            lastPoint = eventData.position;
            inputStarted?.Invoke(eventData.position);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!_pointerDown)
                return;


            Vector2 fromLastP = eventData.position - lastPoint;

            if (lastDirFromBreak == Vector2.zero)
            {
                lastDirFromBreak = fromLastP;
            }

            inputContinued?.Invoke(eventData.position);

            lastPoint = eventData.position;

            float dot = Vector2.Dot(lastDirFromBreak, fromLastP);

            if (dot < 0)
            {
                breakPoints.Add(eventData.position);
                lastDirFromBreak = Vector2.zero;
            }

        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!_pointerDown)
                return;
            OnPointerUp(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!_pointerDown)
                return;

            if (breakPoints.Count == 1)
            {
                inputFinished?.Invoke(tapSign, eventData.position);
                Reset();
                return;
            }

            int brCount = breakPoints.Count;
            Vector2 fromLastBreak = eventData.position - breakPoints[brCount - 1];
            if (fromLastBreak.sqrMagnitude > fromLastBreakThreshold * fromLastBreakThreshold)
            {
                breakPoints.Add(eventData.position);
            }

            InputData inputData = new InputData(breakPoints);
            Sign result = schemContrlr.CheckSchemes(inputData);
            inputFinished?.Invoke(result, eventData.position);
            Reset();
        }

        void Reset()
        {
            breakPoints.Clear();
            lastDirFromBreak = Vector2.zero;
            _pointerDown = false;
        }

        private void OnDestroy()
        {
            Game.fightStarted -= TurnOn;
            Game.fightIsOver -= TurnOff;
        }


        void OnDrawGizmosSelected()
        {

            if (breakPoints != null && breakPoints.Count != 0)
            {
                Gizmos.color = Color.green;
                Vector2 start = breakPoints[0];

                foreach (var item in breakPoints)
                {
                    Gizmos.DrawLine(start, item);
                    Gizmos.DrawWireSphere(item, fromLastBreakThreshold);
                    start = item;
                }

                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(lastPoint, 50f);
            }

            Gizmos.DrawLine(lastPoint, lastPoint + lastDirFromBreak.normalized * 150f);
        }


    }
}
