﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace RunesProj
{
    public class ButtonOnClick : MonoBehaviour, IPointerClickHandler
    {
        public event Action onClick;

        public void OnPointerClick(PointerEventData eventData)
        {
            onClick?.Invoke();
        }
    }
}