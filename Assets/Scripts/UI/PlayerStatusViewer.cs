﻿using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class PlayerStatusViewer : MonoBehaviour
    {
        public PlayerPosTracker posTracker;
        public Slider levelProgressBar;
        public Counter lifesCount;
        public PlayerStatus playerStatus;
        private float playerStartPos;

        void Start()
        {
            posTracker.playerPosChanged += OnPlayePosChanged;
            OnNumberOfLifesChanged(playerStatus.NumberOfLifes);
            playerStatus.numberOfLifesChanged += OnNumberOfLifesChanged;
            levelProgressBar.maxValue = Game.levelSettings.LevelLength;
            levelProgressBar.value = 0f;
        }


        void OnNumberOfLifesChanged(int numberOfLifes)
        {
            lifesCount.SetCount(numberOfLifes);
        }

        void OnPlayePosChanged(float pos)
        {
            levelProgressBar.value = posTracker.PlayerPosZ;
        }

        void OnDestroy()
        {
            playerStatus.numberOfLifesChanged -= OnNumberOfLifesChanged;
        }
    }
}