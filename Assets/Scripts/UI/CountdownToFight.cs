﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class CountdownToFight : MonoBehaviour
    {
        public Text textOutput;
        public int countFrom = 3;
        public string finalMessage = "fight!";
        public float secondScale = .05f;
        private int step;


        void Start()
        {
            StartCountdown();
            Game.fightIsOver += StartCountdown;
        }

        void StartCountdown() { StartCoroutine(Countdown(new WaitForSeconds(secondScale))); }


        IEnumerator Countdown(WaitForSeconds stepTime)
        {

            for (int i = 0; i < countFrom; i++)
            {
                textOutput.text = (countFrom - i).ToString();
                yield return stepTime;
            }
            textOutput.text = finalMessage;
            yield return stepTime;
            textOutput.text = "";

            Game.StartFight();
        }

        void OnDestroy() { Game.fightIsOver -= StartCountdown; }
    }
}