﻿using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class UnitInfoViewer : MonoBehaviour
    {
        public Slider healthBar;

        public UnitInfo unitInfo;

        void Start()
        {
            unitInfo.healthPercentChanged += SetHealthBar;

            SetHealthBar(unitInfo.HealthPercent);
        }

        void SetHealthBar(float percentValue)
        {
            healthBar.value = percentValue;
            // OnUnitInfoValueChanged(healthBar, percentValue); 
        }

        // void OnUnitInfoValueChanged(Slider valueBar, float newValuePercent)
        // {
        //     valueBar.value = newValuePercent;
        //     GameObject barGameObject = valueBar.gameObject;
        //     bool active = barGameObject.activeSelf;

        //     if (newValuePercent <= 0f && active)
        //         barGameObject.SetActive(false);
        //     else if (newValuePercent > 0f && !active)
        //         barGameObject.SetActive(true);
        // }

        void OnDestroy()
        {
            unitInfo.healthPercentChanged -= SetHealthBar;
        }
    }
}