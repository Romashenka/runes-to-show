﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace RunesProj
{
    public class SceneIndicator : MonoBehaviour
    {
        public Text text;

        void Start()
        {
            text.text = Game.levelSettings.name;
        }
    }
}