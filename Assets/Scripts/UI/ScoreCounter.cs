﻿using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class ScoreCounter : MonoBehaviour
    {
        private Text scoreCount;

        void Start()
        {
            Game.scoresChanged += OnScoresChanged;
            scoreCount = GetComponent<Text>();
            OnScoresChanged(Game.Scores, Game.levelSettings.TargetCount);
        }

        void OnScoresChanged(int newScore, int targetScore)
        {
            scoreCount.text = newScore + "/" + targetScore;
        }

        void OnDestroy()
        {
            Game.scoresChanged -= OnScoresChanged;
        }
    }
}