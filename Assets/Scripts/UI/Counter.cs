﻿using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    public Text countText;
    public void SetCount(int count)
    {
        countText.text = count.ToString();
    }
}
