﻿using UnityEngine;
using System.Collections.Generic;


namespace RunesProj
{
    public class UICoordinator : MonoBehaviour
    {
        public GameObject gameOverMenu;

        public GameObject winScreen;

        private Stack<GameObject> modals;

        private void Start()
        {
            modals = new Stack<GameObject>();

            Game.gameOver += OnGameOver;
            Game.winTargetReached += OnWinTargetReached;
            // Game.ResetScores();
        }

        public void OpenModal(GameObject modal)
        {
            if (modals.Count == 0)
                Game.PauseGame();

            modal.SetActive(true);
            modals.Push(modal);
        }

        public void CloseModal(GameObject modal)
        {
            modal.SetActive(false);
            modals.Pop();
            if (modals.Count == 0)
                Game.ContinueGame();
        }

        private void OnGameOver() { OpenModal(gameOverMenu); }

        private void OnWinTargetReached() { OpenModal(winScreen); }

        private void OnDestroy()
        {
            Game.gameOver -= OnGameOver;
            Game.winTargetReached -= OnWinTargetReached;
        }
    }
}