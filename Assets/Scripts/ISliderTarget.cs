﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RunesProj
{
    public interface ISliderTarget
    {
        float CurrentValue { get; }
        float MaxValue { get; }
        event Action<float> valueChanged;
        event Action<float> maxValueChanged;
    }
}