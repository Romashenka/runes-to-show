﻿using System.Collections.Generic;
using UnityEngine;

namespace RunesProj
{
    public class ObjectsUtilizer : MonoBehaviour
    {
        public GroundMarking groundMarking;
        public Transform player;
        private List<Transform> trackingObjects = new List<Transform>();
        private float backBoundZ;

        private void Start()
        {
            backBoundZ = groundMarking.BackBound;
        }

        private void Update()
        {
            if (trackingObjects.Count == 0)
                return;

            Transform eldestObject = trackingObjects[0];
            if (eldestObject == null)
            {
                trackingObjects.RemoveAt(0);
            }
            else if (eldestObject.position.z - player.position.z < backBoundZ)
            {
                trackingObjects.RemoveAt(0);
                Destroy(eldestObject.gameObject);
            }
        }
        public void AddTracking(Transform transform)
        {
            trackingObjects.Add(transform);
        }
    }
}