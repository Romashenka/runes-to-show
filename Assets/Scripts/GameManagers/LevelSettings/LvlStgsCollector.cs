﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RunesProj
{
    [CreateAssetMenu]
    public class LvlStgsCollector : ScriptableObject
    {
        [SerializeField, HideInInspector]
        private List<string> levelSettingsPaths = new List<string>();

        public static int GetLevelsCount()
        { return LoadCollector().levelSettingsPaths.Count; }

        public static LevelSettings GetSceneSettings(int sceneNumber)
        { return GetSceneSettings(sceneNumber, LoadCollector()); }

        public static LevelSettings GetSceneSettings(int sceneNumber, LvlStgsCollector collector)
        {
            string settingsPath = collector.levelSettingsPaths[sceneNumber];
            LevelSettings lS = Resources.Load<LevelSettings>(settingsPath);
            return lS;
        }

        public List<LevelSettings> LoadAllSettings()
        {
            int count = levelSettingsPaths.Count;
            List<LevelSettings> settings = new List<LevelSettings>(count);
            for (int i = 0; i < count; i++)
            {
                settings.Add(Resources.Load<LevelSettings>(levelSettingsPaths[i]));
            }
            return settings;
        }

        public static LvlStgsCollector LoadCollector()
        { return Resources.Load<LvlStgsCollector>(Paths.settingsCollectorPath); }

#if UNITY_EDITOR
        public void SetNewSettingsPath(int index, LevelSettings newSettings)
        { levelSettingsPaths[index] = GetPath(newSettings); }

        public void Insert(int index, LevelSettings settings)
        { levelSettingsPaths.Insert(index, GetPath(settings)); }

        public void Remove(int index)
        { levelSettingsPaths.RemoveAt(index); }

        private string GetPath(LevelSettings lS)
        {
            string newSettPath = AssetDatabase.GetAssetPath(lS);
            newSettPath = Paths.TrimExtension(newSettPath);
            return Paths.TrimResourcesFolderPath(newSettPath);
        }
#endif

    }
}
