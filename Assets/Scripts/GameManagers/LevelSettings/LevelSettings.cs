﻿using UnityEngine;

namespace RunesProj
{
    [CreateAssetMenu(menuName = "LevelSettings")]
    public class LevelSettings : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField, SettingsField(0f, 100f, "Distance To First Enemy")]
        private float _distanceBetweenEnemies = 100f;
        public float DistanceBetweenEnemies { get { return _distanceBetweenEnemies; } }

        [SerializeField, SettingsField(0f, 500f, "Distance To First Enemy")]
        private float _distanceToFirstEnemy = 100f;
        public float DistanceToFirstEnemy { get { return _distanceToFirstEnemy; } }

        [SerializeField, SettingsField(1, 100, "Number Of Enemies on Level")]
        private int _numberOfEnemiesOnLevel = 10;
        public int NumberOfEnemiesOnLevel { get { return _numberOfEnemiesOnLevel; } }


        // [SerializeField, SettingsField(0f, 1f, "Enemy Probability")]
        private float _enemyProbability = .3f;
        public float EnemyProbability { get { return _enemyProbability; } }


        // [SerializeField, SettingsField(0f, 1f, "Wall Probability")]
        private float _wallProbability = .2f;
        public float WallProbability { get { return _wallProbability; } }


        [SerializeField, SettingsField(0, 3, "Max Enemy In Row", true)]
        private int _maxEnemyInRow;
        public int MaxEnemyInRow { get { return _maxEnemyInRow; } }


        [SerializeField, SettingsField(0, 3, "Min Enemy In Row", true)]
        private int _minEnemyInRow;
        public int MinEnemyInRow { get { return _minEnemyInRow; } }


        // [SerializeField, SettingsField(0, 3, "Max Walls In Row", true)]
        private int _wallsMaxInRow;
        public int WallsMaxInRow { get { return _wallsMaxInRow; } }


        // [SerializeField, SettingsField(0, 3, "Min Walls In Row", true)]
        private int _wallsMinInRow;
        public int WallsMinInRow { get { return _wallsMinInRow; } }

        //////////////
        [SerializeField]
        private int _targetCount = 10;
        public int TargetCount { get { return _numberOfEnemiesOnLevel; } }
        ////////////////

        [SerializeField, Range(1f, 100f), SettingsField(1f, 100f, "Move Speed")]
        private float _moveSpeed = 1f;
        public float MoveSpeed { get { return _moveSpeed; } }


        // [SerializeField, Range(1f, 20f), SettingsField(1f, 20f, "Next Enemy Time")]
        private float _nextEnemyTimeMin = 3f;
        public float NextEnemyTime { get { return _nextEnemyTimeMin; } }

        public float EnemiesDistanceMin { get { return _nextEnemyTimeMin * _moveSpeed; } }


        [SerializeField, Range(1f, 20f)]
        private float _nextEnemyTimeMax = 5f;
        public float EnemiesDistanceMax { get { return _nextEnemyTimeMax * _moveSpeed; } }


        // [SerializeField, Range(1f, 10000f), SettingsField(1f, 10000f, "Level Lenght")]
        private float _levelLength = 500f;
        public float LevelLength { get { return _levelLength; } }


        [SerializeField]
        private Sign[] _validEnemySigns = new Sign[0];
        public Sign[] ValidEnemySigns { get; private set; }

        [SerializeField]
        private Sign[] _controlSigns = new Sign[0];
        public Sign[] ControlSigns { get; private set; }


        [SerializeField]
        private RefSetTransform environmentSet = null;

        [SerializeField]
        private EnemiesSet _avaliableEnemies = null;
        public EnemiesSet AvaliableEnemies { get { return _avaliableEnemies; } }

        public Sign[] AllValidSigns()
        {
            int len = _validEnemySigns.Length + _controlSigns.Length;
            Sign[] allSigns = new Sign[len];
            _controlSigns.CopyTo(allSigns, 0);
            _validEnemySigns.CopyTo(allSigns, _controlSigns.Length);
            return allSigns;
        }


        public void GetEnvObjects(out Transform[] objsRef)
        {
            Transform[] objects = environmentSet.objects;
            objsRef = new Transform[objects.Length];
            objects.CopyTo(objsRef, 0);
        }

        public void OnBeforeSerialize() { }

        public void OnAfterDeserialize()
        {
            ValidEnemySigns = new Sign[_validEnemySigns.Length];
            _validEnemySigns.CopyTo(ValidEnemySigns, 0);

            ControlSigns = new Sign[_controlSigns.Length];
            _controlSigns.CopyTo(ControlSigns, 0);
        }

    }

    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class SettingsFieldAttribute : System.Attribute
    {
        public readonly float maxValue;
        public readonly float minValue;
        public readonly string name;
        public readonly bool useWholeNumbers;

        // readonly string positionalString;

        // This is a positional argument

        public SettingsFieldAttribute(float min, float max, string name, bool useWholeNumbers = false)
        {
            maxValue = max;
            minValue = min;
            this.name = name;
            this.useWholeNumbers = useWholeNumbers;
        }
        // public SettingsFieldAttribute(int min, int max, string name)
        // {
        //     return new SettingsFieldAttribute(min, max, name);
        // }

        // public string PositionalString
        // {
        //     get { return positionalString; }
        // }

        // // This is a named argument
        // public int NamedInt { get; set; }
    }
}