﻿using System;
using UnityEngine;

namespace RunesProj
{
    [Serializable]
    public class EnemiesSet
    {
        [SerializeField]
        private Element[] enemies;
        public Element[] Enemies
        { get { Element[] ens = new Element[enemies.Length]; enemies.CopyTo(ens, 0); return ens; } }

        public Enemy RandomEnemy
        {
            get
            {
                int rand = UnityEngine.Random.Range(0, enemies.Length);
                return enemies[rand].enemyPrefab;
            }
        }

        [Serializable]
        public struct Element
        {
            public Element(Enemy prefab, int count)
            {
                enemyPrefab = prefab;
                this.count = count;
            }

            public Enemy enemyPrefab;
            public int count;
        }
    }
}