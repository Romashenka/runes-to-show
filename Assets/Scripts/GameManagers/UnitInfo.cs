﻿using System;
using UnityEngine;

namespace RunesProj
{
    [CreateAssetMenu]
    public class UnitInfo : ScriptableObject
    {
        [SerializeField]
        private float healthInitial = 100f;
        public float HealthInitial { get { return healthInitial; } }

        private float healthCurrent;
        public float HealthCurrent
        {
            get => healthCurrent;
            set
            {
                healthCurrent = value;
                healthPercentChanged?.Invoke(HealthPercent);
            }
        }
        public float HealthPercent { get => healthCurrent / healthInitial; }

        public event Action<float> healthPercentChanged;

        public void Reset()
        {
            HealthCurrent = HealthInitial;
        }
        
        public void SetInitialHealth(float helath)
        {
            healthInitial = helath;
        }
    }
}