﻿using UnityEngine;
using System.Collections.Generic;

namespace RunesProj
{
    public class GroundGrid
    {
        private int zSize = 30;
        private int xSize = 3;

        private float cellSize = 5f;
        private Vector2 spacing;

        [SerializeField]
        public int[,] grid;


        private float enemyProb;
        private float wallProb;
        private int enemiesOnLevel;

        private int enemiesMinInRow;
        private int enemiesMaxInRow;

        private float HalfCell { get { return cellSize * .5f; } }
        private float CellWithSpacingX { get { return cellSize + spacing.x; } }
        private Vector3 Offset
        { get { return new Vector3(CellWithSpacingX * .5f * xSize - spacing.x * .5f, 0f, HalfCell); } }

        public GroundGrid(int xSize, int zSize, float cellSize, Vector2 spacing, LevelSettings settings)
        {
            this.xSize = xSize;
            this.zSize = zSize;
            this.cellSize = cellSize;
            this.spacing = spacing;
            // this.enemyProb = enemyProb;
            // this.wallProb = wallProb;
            enemiesOnLevel = settings.NumberOfEnemiesOnLevel;
            enemiesMinInRow = settings.MinEnemyInRow;
            enemiesMaxInRow = settings.MaxEnemyInRow;
            ResetGrid();
        }

        public Vector3 CellPos(int xIndex, int zIndex)
        {
            return new Vector3(xIndex * CellWithSpacingX + HalfCell, 0f, zIndex * (cellSize + spacing.y) + HalfCell) - Offset;
        }

        public float XCoord(int index)
        {
            return CellWithSpacingX * index + HalfCell - Offset.x;
        }

        public int GridPosX(float xCoord)
        {
            float xOffset = Offset.x;
            xCoord += xOffset - HalfCell;
            return Mathf.RoundToInt(xCoord / CellWithSpacingX);
        }


        public void ResetGrid()
        {
            grid = new int[xSize, zSize];
            SetMainWay();
            SetWalls();
            SetEnemies();
        }

        void SetMainWay()
        {
            int zLen = grid.GetLength(1);
            int currentX = 1;
            for (int i = 1; i < zLen; i++)
            {
                int nextMoveX = Random.Range(-1, 2);
                currentX += nextMoveX;
                currentX = Mathf.Clamp(currentX, 0, 2);
                grid[currentX, i] = 1;
                grid[currentX, i - 1] = 1;
            }
        }

        void SetWalls()
        {
            int xLen = grid.GetLength(0);
            int zLen = grid.GetLength(1);
            for (int i = 0; i < xLen; i++)
            {
                for (int j = 0; j < zLen; j++)
                {
                    if (grid[i, j] == 0)
                    {
                        int rand = RandomProb(wallProb);
                        grid[i, j] = rand * 2;
                    }
                }
            }
        }

        void SetEnemies()
        {
            int xLen = grid.GetLength(0);
            int zLen = grid.GetLength(1);

            for (int i = 0; i < xLen; i++)
            {
                for (int j = 2; j < zLen; j++)
                {
                    if (grid[i, j] != 2)
                    {
                        int rand = RandomProb(enemyProb);
                        grid[i, j] = rand * 3;
                    }
                }
            }
        }


        private int RandomProb(float probability)
        {
            if (Random.Range(0f, 1f) < probability)
                return 1;
            else
                return 0;
        }


        public void DrawGrid(Vector3 center)
        {
            if (grid != null)
            {
                int lenX = grid.GetLength(0);
                int lenZ = grid.GetLength(1);
                for (int i = 0; i < lenX; i++)
                {
                    for (int j = 0; j < lenZ; j++)
                    {
                        if (grid[i, j] == 1)
                        {
                            Gizmos.color = Color.blue;
                        }
                        else if (grid[i, j] == 2)
                        {
                            Gizmos.color = Color.green;
                        }
                        else if (grid[i, j] == 3)
                        {
                            Gizmos.color = Color.red;
                        }

                        Vector3 pos = CellPos(i, j); //new Vector3(i, 0f, j) * cellSize;
                        Gizmos.DrawWireSphere(pos, cellSize / 2f);
                        Gizmos.color = Color.white;
                    }
                }
            }
        }

        public void Draw()
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 pos = new Vector3(XCoord(i), 0f, 0f);
                Gizmos.DrawWireSphere(pos, 3f);
            }
        }

    }
}