﻿using System;
using UnityEngine;

namespace RunesProj
{
    public class PlayerPosTracker : MonoBehaviour
    {
        public Transform player;

        private float playerStartZ;

        private float _levelLength;
        
        private float _playerPosZ;
        public float PlayerPosZ
        {
            get { return _playerPosZ; }
            private set
            {
                _playerPosZ = value;
                playerPosChanged?.Invoke(_playerPosZ);
                if (_playerPosZ >= _levelLength)
                {
                    playerReachedLevelLength?.Invoke();
                    Game.WinReached();
                }
            }
        }
        public event Action<float> playerPosChanged;
        public event Action playerReachedLevelLength;

        void Start()
        {
            playerStartZ = player.position.z;
            _levelLength = Game.levelSettings.LevelLength;
        }

        void Update()
        {
            float currentPos = player.position.z;
            if (PlayerPosZ != currentPos)
                PlayerPosZ = currentPos;
        }

        public float GetPlayerPosition()
        {
            return player.position.z;
        }
    }
}