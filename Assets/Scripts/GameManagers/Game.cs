﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RunesProj
{
    public static class Game
    {
        public static event Action gameOver;
        public static event Action<int, int> scoresChanged;
        public static event Action winTargetReached;
        public static event Action fightStarted;
        public static event Action fightIsOver;
        public static LevelSettings levelSettings { get; private set; }
        public static int currentLevelIndex { get; private set; }

        private static int scores;
        public static int Scores
        {
            get { return scores; }
            set
            {
                scores = value;
                int target = levelSettings.TargetCount;
                scoresChanged?.Invoke(value, target);
                if (scores >= target)
                {
                    WinReached();
                }
            }
        }

        public static void StartFight() => fightStarted?.Invoke();
        public static void EndFight() => fightIsOver?.Invoke();

        public static void PauseGame() => Time.timeScale = 0f;
        public static void ContinueGame() => Time.timeScale = 1f;


        public static void GameOver()
        {
            Time.timeScale = 0f;
            scores = 0;
            if (gameOver != null)
                gameOver();
        }


        public static void WinReached()
        {
            winTargetReached?.Invoke();
        }

        // public static void AddScores()
        // {
        //     Scores--;
        //     if (scores == 0)
        //     {
        //         Time.timeScale = 0f;
        //         winScoresReached?.Invoke();
        //     }
        // }

        private static void ChangeLevelSettings(LevelSettings newOne)
        {
            levelSettings = newOne;
            // Scores = newOne.TargetCount;
        }


        public static void GoToLevel(int levelIndex)
        {
            SceneManager.LoadScene(1);
            LevelSettings ls = LvlStgsCollector.GetSceneSettings(levelIndex);
            ChangeLevelSettings(ls);
            currentLevelIndex = levelIndex;
            Scores = 0;
        }

#if UNITY_EDITOR
        public static void StartLevel(LevelSettings settings)
        {
            Scene s = SceneManager.GetActiveScene();
            SceneManager.LoadScene(s.name);
            ChangeLevelSettings(settings);
        }
#endif

    }

}