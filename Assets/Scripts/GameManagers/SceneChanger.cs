﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace RunesProj
{
    [CreateAssetMenu]
    public class SceneChanger : ScriptableObject
    {

        public void ExitGame()
        {
            Application.Quit();
        }


#if UNITY_EDITOR

        public void RestartScene()
        {
            // Game.GoToLevel(Game.currentLevelIndex);
            Scene s = SceneManager.GetActiveScene();
            SceneManager.LoadScene(s.name);
        }
#else
        public void RestartScene()
        {
            Game.GoToLevel(Game.currentLevelIndex);
        }
#endif
        public void LoadMainMenu()
        {
            SceneManager.LoadScene(0);
        }

        public void StartNewGame()
        {
            Game.GoToLevel(0);
        }

    }
}