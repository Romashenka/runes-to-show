﻿using UnityEngine;
using RunesProj.DrawingInput;

namespace RunesProj
{
    [CreateAssetMenu(menuName = "DrawingInput/Sign")]
    public class Sign : ScriptableObject
    {
        [SerializeField]
        private Projectile _projectilePrefab;
        public Projectile ProjectilePrefab { get { return _projectilePrefab; } }

        [SerializeField]
        private Vector2Int[] _unitsPositions = new Vector2Int[0];
        public Vector2Int[] UnitsPositions
        {
            get
            {
                Vector2Int[] pos = new Vector2Int[_unitsPositions.Length];
                _unitsPositions.CopyTo(pos, 0);
                return pos;
            }
        }

        [SerializeField]
        private float _unitsArea = 18f;
        public float unitsArea { get { return _unitsArea; } }

        // [SerializeField]
        // private Sprite _signSprite = null;
        // public Sprite SignSprite { get { return _signSprite; } }

        [SerializeField]
        private Enemy[] _enemies = null;
        public Enemy EnemyRand
        { get { int rand = Random.Range(0, _enemies.Length); return _enemies[rand]; } }


        [Header("Input Schem")]
        [SerializeField]
        private bool directed = true;
        public bool IsControlSign { get { return directed; } }

        [SerializeField, Range(2, 10)]
        private int _numberOfPoints = 2;

        [SerializeField]
        private Vector2 startDirection = Vector2.up;
        public float directionX { get { return startDirection.x; } }

        [SerializeField]
        private Vector2 offset;

        public IInputSchem Schem
        { get { return InputSchemesCreator.GetScheme(_numberOfPoints, startDirection, offset, directed); } }


        public void OnAfterDeserialize()
        {
            if (startDirection.x != 0f)
            {
                startDirection.y = 0f;
                offset.x = 0f;
            }
            else if (startDirection.y != 0f)
            {
                startDirection.x = 0f;
                offset.y = 0f;
            }
        }

        public void OnBeforeSerialize() { }
    }
}