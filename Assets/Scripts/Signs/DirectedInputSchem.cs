﻿using System.Collections.Generic;
using UnityEngine;

namespace RunesProj.DrawingInput
{

    public class DirectedInputSchem : IInputSchem
    {

        private Vector2 startDirection;
        private int numberOfPoints;

        public DirectedInputSchem(int numberOfPoints, Vector2 startDirection)
        {
            this.numberOfPoints = numberOfPoints;
            this.startDirection = startDirection;
        }

        public bool CheckSchem(InputData inputData)
        {
            List<Vector2> breakPoints = inputData.breakPoints;
            int len = breakPoints.Count;

            if (numberOfPoints == len)
            {
                Vector2 firstVec = (breakPoints[1] - breakPoints[0]).normalized;
                firstVec.Set(Mathf.Round(firstVec.x), Mathf.Round(firstVec.y));
                if (firstVec.x == startDirection.x && firstVec.y == startDirection.y)
                    return true;
            }
            return false;
        }

        private Vector2 MultAxes(Vector2 v1Abs, Vector2 v2)
        {
            return new Vector2(Mathf.Abs(v1Abs.x) * v2.x, Mathf.Abs(v1Abs.y) * v2.y);
        }



    }

}
