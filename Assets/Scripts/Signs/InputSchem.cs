﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace RunesProj.DrawingInput
{
    public abstract class InputSchem
    {
        public abstract bool CheckSchem();
    }
}