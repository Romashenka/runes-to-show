﻿using UnityEngine;


namespace RunesProj.DrawingInput
{
    public static class InputSchemesCreator
    {
        public static IInputSchem GetScheme(int numberOfPoints, Vector2 startDirection, Vector2 offset, bool directed)
        {
            if (directed)
                return new DirectedInputSchem(numberOfPoints, startDirection);
            else
                return new NonDirectedSchem(numberOfPoints, startDirection, offset);
            // return new DirectedInputScheme()   
        }
    }
}