﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RunesProj.DrawingInput
{
    public interface IInputSchem
    {
        bool CheckSchem(InputData inputData);
    }
}