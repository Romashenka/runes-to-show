﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace RunesProj.DrawingInput
{
    public class NonDirectedSchem : IInputSchem
    {   private readonly int numberOfPoints;

        private readonly Vector2 startDirection;

        private readonly Vector2 offset;

        public NonDirectedSchem(int numberOfPoints, Vector2 startDirection, Vector2 offset)
        {
            this.numberOfPoints = numberOfPoints;
            this.startDirection = new Vector2(Math.Sign(startDirection.x), Math.Sign(startDirection.y));
            this.offset = new Vector2(Math.Sign(offset.x), Math.Sign(offset.y));
        }

        public bool CheckSchem(InputData inputData)
        {
            List<Vector2> breakPoints = inputData.breakPoints;

            int len = breakPoints.Count;
            Vector2 firstVec = (breakPoints[1] - breakPoints[0]).normalized;
            firstVec.Set(Mathf.Round(firstVec.x), Mathf.Round(firstVec.y));
            firstVec = MultAxes(startDirection, firstVec);

            if (numberOfPoints == len)
            {
                bool axisMatch = true;
                bool offsetMatch = true;
                if (numberOfPoints % 2 == 0)
                {
                    axisMatch = firstVec.x != 0 || firstVec.y != 0;
                    float offsetMultiplyer = (firstVec.x + firstVec.y) * (startDirection.x + startDirection.y);
                    offsetMatch = CheckOffset(breakPoints, offset, offsetMultiplyer);
                }
                else
                {
                    axisMatch = firstVec == startDirection;
                    offsetMatch = CheckOffset(breakPoints, offset);
                }
                return axisMatch && offsetMatch;
            }
            else
                return false;
        }

        private Vector2 MultAxes(Vector2 v1Abs, Vector2 v2)
        {
            return new Vector2(Mathf.Abs(v1Abs.x) * v2.x, Mathf.Abs(v1Abs.y) * v2.y);
        }

        private bool CheckOffset(List<Vector2> breakPoints, Vector2 offset, float offsetMultiplyer)
        {
            int len = breakPoints.Count;
            bool offsetMatch = true;
            for (int i = 0; i < len - 2; i++)
            {
                Vector2 dirI = breakPoints[i + 2] - breakPoints[i];
                dirI = MultAxes(offset, dirI);
                dirI.x = Math.Sign(dirI.x);
                dirI.y = Math.Sign(dirI.y);
                offsetMatch = offsetMatch && dirI == offset * offsetMultiplyer;
            }
            return offsetMatch;
        }

        private bool CheckOffset(List<Vector2> breakPoints, Vector2 offset)
        {
            int len = breakPoints.Count;
            bool offsetMatch = true;
            // float prevSign 
            for (int i = 1; i < len - 2; i++)
            {
                Vector2 dirPrev = breakPoints[i - 1] - breakPoints[i + 1];
                dirPrev = MultAxes(offset, dirPrev);

                Vector2 dirI = breakPoints[i] - breakPoints[i + 2];
                dirI = MultAxes(offset, dirI);
                offsetMatch = offsetMatch
                              &&
                              Math.Sign(dirI.x) == Math.Sign(dirPrev.x)
                              &&
                              Math.Sign(dirI.y) == Math.Sign(dirPrev.y);
                dirPrev = dirI;
            }
            return offsetMatch;
        }
    }
}