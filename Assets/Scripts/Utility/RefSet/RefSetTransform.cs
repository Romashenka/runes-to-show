﻿using UnityEngine;

namespace RunesProj
{
    [CreateAssetMenu(menuName = "RefSet/Transforms")]
    public class RefSetTransform : RefSet<Transform> { }
}