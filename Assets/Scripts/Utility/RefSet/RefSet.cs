﻿using UnityEngine;

namespace RunesProj
{
    public class RefSet<T> : ScriptableObject
    {
        public T[] objects;
    }
}