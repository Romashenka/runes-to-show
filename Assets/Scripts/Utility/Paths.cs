﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RunesProj
{
    public static class Paths
    {
        public static readonly string resourcesFolderPath = "Assets/Resources/";
        public static readonly string settingsCollectorPath = "LvlSettings/LvlSet";


        public static string TrimResourcesFolderPath(string path)
        {
            return path.Trim(resourcesFolderPath.ToCharArray());
        }

        public static string TrimExtension(string path)
        {
            int i = path.LastIndexOf('.');
            if (i != -1)
                return path.Remove(i);
            else
                return path;
        }

        public static string PathToFileName(string path)
        {
            int i = path.LastIndexOf('/');
            path = path.Remove(0, i + 1);
            return TrimExtension(path);
        }
    }
}