﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{

    public static Vector3 ScreenToWorld(Camera cam, Vector3 screenPos)
    {
        screenPos.z = -cam.transform.position.z;
        return cam.ScreenToWorldPoint(screenPos);
    }

    public static Vector3 WorldToScreen(Camera cam, Vector3 worldPos)
    {
        // worldPos.z = cam.transform.position.z;
        return cam.WorldToScreenPoint(worldPos);
    }

    public static T RandomElement<T>(T[] arrey)
    {
        float length = arrey.Length;
        float chance = 1f / length;
        float rand = Random.value;
        int index = (int)(rand / chance);
        return arrey[index];
    }
}
