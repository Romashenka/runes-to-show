﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public GameObject TargetGameObject;
    public Slider _slider;

    private IHealthBarTarget _target;


    void Start()
    {
        _target = TargetGameObject.GetComponent<IHealthBarTarget>();
        _slider.maxValue = _target.MaxHealth;
        _slider.value = _target.CurrentHealth;
        _target.HealthChanged += (int health) => _slider.value = health;
    }
}

public interface IHealthBarTarget
{
    int MaxHealth{ get; }
    int CurrentHealth { get; }
    event Action<int> HealthChanged;
}
