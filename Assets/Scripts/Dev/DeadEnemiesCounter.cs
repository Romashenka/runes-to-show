﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeadEnemiesCounter : MonoBehaviour
{
    //public Text Text;
    public Slider Slider;
    public EnemiesCreator _enemiesCreator;
    private float _levelWeight;

    void Start()
    {
        _levelWeight = _enemiesCreator.EnemiesWeightOnLevel;
        Slider.maxValue = _levelWeight;
        Slider.value = 0f;
        _enemiesCreator.EnemiesDeadWeightChanged += SetSliderVal;

    }

    private void SetSliderVal(float value)
    {
        Slider.value = value;
    }

    /*
    private void SetText(int currentDead)
    {
        Text.text = $"{currentDead}/{_enemiesOnLevel}";

    }
    */
}
