﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthText : MonoBehaviour
{
    public GameObject TargetGameObject;
    public Text Text;
    private int _maxHp;
    

    void Start()
    {
        var target = TargetGameObject.GetComponent<IHealthBarTarget>();
        _maxHp = target.MaxHealth;
        SetText(target.CurrentHealth);
        target.HealthChanged += SetText;
    }

    void SetText(int currentHP)
    {
        Text.text = $"{currentHP}/{_maxHp}";
    }

}
