﻿using System.Collections;
using System.Collections.Generic;
using RunesProj;
using RunesProj.DrawingInput;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public HomeArea HomeArea;

    public InputDir DrawingInput;
    public HomeAreaInput HomeAreaInput;

    public Sign FreezeSign;
    public Sign TapSign;
    public GameObject FreezeBallPrefab;
    public GameObject FireballPrefab;
    public GameObject FireBombPrefab;
    public float FireBombChargeTime = 1.5f;
    public float SpellsVelocity = 3f;

    private GameObject _chargedSignSpell;

    private Rigidbody2D _instantiatedSpell;

    private Coroutine _spellChargeCoroutine;

    private Vector2 _startPoint;

    private Camera _mainCam;
    private float _depth;

    void Start()
    {
        _mainCam = Camera.main;
        _depth = Mathf.Abs(_mainCam.transform.position.z);
        /*HomeAreaInput.Inputed += OnHomeAreaInputed;
        HomeAreaInput.PointerDown += OnHomeAreaPointerDown;
        HomeAreaInput.PointerDrag += OnHomeAreaDragged;
        */
        DrawingInput.inputStarted += OnDrawingInputStarted;
        DrawingInput.inputContinued += OnDrawingInputContinued;

        DrawingInput.inputFinished += OnDrawingInputFinised;
    }




    void OnDrawingInputFinised(Sign sign, Vector2 pos)
    {
        pos = GetWorldPos(pos);

        if (sign == FreezeSign)
        {
            _chargedSignSpell = FreezeBallPrefab;
            HomeArea.ShowCharged();
            Destroy(_instantiatedSpell.gameObject);
        }
        else
        {
            StopCoroutine(_spellChargeCoroutine);
            Vector2 dir = (pos - _startPoint).normalized;
            _instantiatedSpell.velocity = dir * SpellsVelocity;

            if (dir == Vector2.zero)
                Destroy(_instantiatedSpell.gameObject);
            _instantiatedSpell = null;
        }
    }


    void OnDrawingInputContinued(Vector2 pos)
    {
        pos = GetWorldPos(pos);
        if (_instantiatedSpell != null)
        {
            _instantiatedSpell.MovePosition(pos);
            StopCoroutine(_spellChargeCoroutine);
        }
    }


    void OnDrawingInputStarted(Vector2 pos)
    {
        pos = GetWorldPos(pos);
        HomeArea.HideCharged();

        GameObject instance;

        if (_chargedSignSpell == null)
        {
            instance = Instantiate(FireballPrefab, pos, FireballPrefab.transform.rotation);
            _spellChargeCoroutine = StartCoroutine(ChargeSpell());
        }
        else
        {
            instance = Instantiate(_chargedSignSpell, pos, _chargedSignSpell.transform.rotation);
            _chargedSignSpell = null;
        }

        _instantiatedSpell = instance.GetComponent<Rigidbody2D>();
        _startPoint = pos;
        /*
        if (_chargedSignSpell != null)
        {
            instance = Instantiate(FreezeBallPrefab, pos, FreezeBallPrefab.transform.rotation);
            _chargedSignSpell = null;
            HomeArea.HideCharged();
        }
        else
        {
            instance = Instantiate(FireballPrefab, pos, FireballPrefab.transform.rotation);
            _spellChargeCoroutine = StartCoroutine(ChargeSpell());
        }
        _instantiatedSpell = instance.GetComponent<Rigidbody2D>();*/
    }

    private IEnumerator ChargeSpell()
    {
        yield return new WaitForSeconds(FireBombChargeTime);
        Vector3 pos = _instantiatedSpell.transform.position;
        Destroy(_instantiatedSpell.gameObject);
        var instance = Instantiate(FireBombPrefab, pos, FireBombPrefab.transform.rotation);
        _instantiatedSpell = instance.GetComponent<Rigidbody2D>();
    }

    void OnHomeAreaDragged(Vector3 pos)
    {
        _instantiatedSpell?.MovePosition(pos);
    }

    /*void OnHomeAreaInputed(Vector3 pos, Vector3 dir)
    {
        if (_spellChargeCoroutine != null)
            StopCoroutine(_spellChargeCoroutine);
        _instantiatedSpell.velocity = dir * SpellsVelocity;
        _instantiatedSpell = null;
    }
    */

    private Vector3 GetWorldPos(Vector3 pos)
    {
        pos.z = _depth;
        return _mainCam.ScreenToWorldPoint(pos);
    }

    void OnHomeAreaInputed(Vector3 pos, Vector3 dir)
    {
        if (_spellChargeCoroutine != null)
            StopCoroutine(_spellChargeCoroutine);
        _instantiatedSpell.velocity = dir * SpellsVelocity;
        _instantiatedSpell = null;
    }
}
