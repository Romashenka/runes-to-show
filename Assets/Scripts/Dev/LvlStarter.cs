﻿using UnityEngine;


namespace RunesProj
{
    public class LvlStarter : MonoBehaviour
    {

#if UNITY_EDITOR
        public LevelSettings currentSettings;

        void Awake()
        {
            if (Game.levelSettings == null)
                Game.StartLevel(currentSettings);
        }
#endif

    }
}