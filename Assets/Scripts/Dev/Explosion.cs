﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public int Damage;
    public LayerMask EnemiesLayer;
    public float VisualTime;


    void OnTriggerEnter2D(Collider2D collider)
    {
        Enemy enemy = collider.GetComponent<Enemy>();
        HitEnemy(enemy);
        Destroy(gameObject,VisualTime);
    }

    protected virtual void HitEnemy(Enemy enemy)
    {
        enemy.GetDamage(Damage);
    }
}
