﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBomb : Spell
{
    public GameObject BombPrefab;

    protected override void HitEnemy(Enemy enemy)
    {
        Instantiate(BombPrefab, transform.position, BombPrefab.transform.rotation);
    }
}
