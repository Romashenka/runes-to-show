﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour
{
    public int Damage = 10;

    void OnTriggerEnter2D(Collider2D collider)
    {
        Enemy enemy;

        if (collider.gameObject.TryGetComponent<Enemy>(out enemy))
        {
            HitEnemy(enemy);
            Destroy(gameObject);
        }
    }

    protected virtual void HitEnemy(Enemy enemy)
    {
        enemy.GetDamage(Damage);
    }
}
