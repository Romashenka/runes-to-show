﻿using System;
using System.Collections;
using System.Collections.Generic;
using RunesProj;
using UnityEngine;

public class HomeArea : MonoBehaviour, IHealthBarTarget
{
    [SerializeField] private RuneIndicator _runeIndicator;

    [field: SerializeField] public int MaxHealth { get; private set; } = 100;
    private int _currentHealth;
    public int CurrentHealth { get => _currentHealth; set { _currentHealth = value; HealthChanged?.Invoke(value); } }

    public event Action<int> HealthChanged;

    void Start()
    {
        CurrentHealth = MaxHealth;
    }

    public void GetDamage(int damage)
    {
        CurrentHealth -= damage;
        if (CurrentHealth <= 0)
            Die();
    }


    public void HideCharged() => _runeIndicator.HideSign();
    public void ShowCharged() => _runeIndicator.ShowSign();

    private void Die()
    {
        Game.GameOver();
    }
}
