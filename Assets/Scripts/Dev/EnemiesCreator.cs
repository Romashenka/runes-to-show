﻿using System;
using System.Collections;
using System.Collections.Generic;
using RunesProj;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemiesCreator : MonoBehaviour
{
    
    public GameLevels _gameLevels;

    public float EnemiesWeightOnLevel => _currentLevel.LevelWeight;
    public float SpawnAreaLimits = 5f;

    public event Action<float> EnemiesDeadWeightChanged;
    private float _enemiesDead;
    public float EnemiesDeadWeight { get => _enemiesDead; set { _enemiesDead = value; EnemiesDeadWeightChanged?.Invoke(_enemiesDead); } }

    public Transform HUD;


    private List<Enemy> _spawned = new List<Enemy>(20);

    private LevelData _currentLevel;

    private float _currentWaveSpawnedWeight;

    private int _currentWaveIndex;

    private bool _levelSpawned;

    void Awake()
    {
        Time.timeScale = 1f;

        _currentLevel = _gameLevels.GetLevel(0);

        StartCoroutine(SpawnWaves());
    }

    IEnumerator SpawnWaves()
    {
        var wave = _currentLevel.GetWave(_currentWaveIndex);
        _currentWaveSpawnedWeight = 0f;

        SpawnGroup(wave);
        while (_currentWaveSpawnedWeight < wave.WaveWeight)
        {
            yield return new WaitForSeconds(wave.NextGroupTime());
            SpawnGroup(wave);
        }

        if (_currentWaveIndex < _currentLevel.WavesCount)
        {
            yield return new WaitForSeconds(_currentLevel.TimeBetweenWaves);
            _currentWaveIndex++;
            StartCoroutine(SpawnWaves());
        }
        else
            _levelSpawned = true;
    }

    private void SpawnGroup(LevelWave wave)
    {
        var nextGroup = wave.NextEnemiesGroup(_currentWaveSpawnedWeight);
        var nextGroupWeight = nextGroup.GroupSize * nextGroup.EnemyData.EnemyWeight;
        _currentWaveSpawnedWeight += nextGroupWeight;

        Vector3 pos = transform.position;

        for (int i = 0; i < nextGroup.GroupSize; i++)
        {
            Vector3 randPos = pos + Vector3.right * Random.Range(-SpawnAreaLimits, SpawnAreaLimits);
            var enemyPrefab = nextGroup.EnemyData.EnemyPrefab;
            var enemyInstance = enemyPrefab.Spawn(randPos, HUD, nextGroup.EnemyData.EnemyWeight);
            _spawned.Add(enemyInstance);
            enemyInstance.Destryed += OnEnemyDestroyed;
        }
    }


    /*
    IEnumerator SpawnEnemies()
    {
        while (_spawnedEnemiesCount < EnemiesOnLevel)
        {
            Vector3 pos = transform.position;
            Vector3 randPos = pos + Vector3.right * Random.Range(-SpawnAreaLimits, SpawnAreaLimits);

            Enemy randPrefab = Utility.RandomElement<Enemy>(EnemiesPrefabs);
            Enemy instance = randPrefab.Spawn(randPos, HUD);
            _spawnedEnemiesCount++;
            _spawned.Add(instance);
            instance.Destryed += OnEnemyDestroyed;
            float waitTime = Random.Range(EnemiesSpawnPeriodMin, EnemiesSpawnPeriodMax);
            yield return new WaitForSeconds(waitTime);
        }
    }
    */

    private void OnEnemyDestroyed(Enemy enemy)
    {
        EnemiesDeadWeight += enemy.Weight;
        _spawned.Remove(enemy);
        if (_levelSpawned && _spawned.Count == 0)
            Game.WinReached();
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + Vector3.right * SpawnAreaLimits, 1f);
        Gizmos.DrawWireSphere(transform.position - Vector3.right * SpawnAreaLimits, 1f);
    }
}
