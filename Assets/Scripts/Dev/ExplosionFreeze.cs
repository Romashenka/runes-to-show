﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFreeze : Explosion
{
    public float FreezeTime;

    protected override void HitEnemy(Enemy enemy)
    {
        base.HitEnemy(enemy);
        enemy.Freeze(FreezeTime);
    }
}
