﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIElemFollowWorldObject : MonoBehaviour
{
    private Transform TargetPos;
    private Camera mainCam;

    public UIElemFollowWorldObject SetTo(Transform target, Transform hud)
    {
        var instance = Instantiate(this, hud);
        instance.TargetPos = target;
        return instance;
    }

    void Start()
    {
        mainCam = Camera.main;    
    }

    void Update()
    {
        if (TargetPos == null)
        {
            Destroy(gameObject);
            return;
        }

        transform.position = mainCam.WorldToScreenPoint(TargetPos.position);
    }
}
