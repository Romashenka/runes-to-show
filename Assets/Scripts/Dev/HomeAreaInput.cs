﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HomeAreaInput : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler, IPointerExitHandler
{
    public Transform _worldGameplayPlane;

    private Vector3 _startTapPoint;

    private Camera _mainCam;
    private float _depth;

    public event Action<Vector3, Vector3> Inputed;
    public event Action<Vector3> PointerDown;
    public event Action<Vector3> PointerDrag;

    private bool _pointerIsDown;

    private Rect _rect;
    private RectTransform _rectTransform;

    private void Start()
    {
        _rect = GetComponent<RectTransform>().rect;
        _mainCam = Camera.main;
        _depth = Mathf.Abs(_mainCam.transform.position.z);
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        _pointerIsDown = true;
        _startTapPoint = GetWorldPos(eventData.position);
        PointerDown(_startTapPoint);
    }

    public void OnDrag(PointerEventData eventData)
    {
        PointerDrag(GetWorldPos(eventData.position));
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnPointerUp(eventData);
    }


    public void OnPointerUp(PointerEventData eventData)
    {
        if (_pointerIsDown)
        {
            _pointerIsDown = false;
            Vector3 _endPos = GetWorldPos(eventData.position);
            Vector3 dir = (_endPos - _startTapPoint).normalized;
            Inputed(_startTapPoint, dir);
        }
    }

    private Vector3 GetWorldPos(Vector3 pos)
    {
        pos.z = _depth;
        return _mainCam.ScreenToWorldPoint(pos);
    }


    void OnDrawGizmos()
    {
        RectTransform rt = GetComponent<RectTransform>();
        Rect rect = rt.rect;


    }


}
