﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IHealthBarTarget
{
    [field: SerializeField] public int MaxHealth { get; private set; } = 10;
    public int Damage = 20;
    public float WaitTimeWhenDamaged = .5f;
    public float Speed = 8f;

    private int _currentHealth;
    public int CurrentHealth { get => _currentHealth; set { _currentHealth = value; HealthChanged?.Invoke(_currentHealth); } }
    public event Action<int> HealthChanged;

    public SpriteRenderer DamagedSprite;
    public SpriteRenderer FreezeSprite;

    public Transform HealthBarPos;

    public UIElemFollowWorldObject HealthBarPrefab;
    private Transform _hud;

    private Coroutine _dameged;
    private Coroutine _freezed;

    public event Action<Enemy> Destryed;

    public float Weight { get; private set; }

    public Enemy Spawn(Vector3 pos, Transform hud, float weight)
    {
        Enemy enemy = Instantiate(this, pos, transform.rotation);
        enemy._hud = hud;
        enemy.Weight = weight;
        return enemy;
    }

    void Start()
    {
        var healthBarInstance = HealthBarPrefab.SetTo(HealthBarPos, _hud);
        healthBarInstance.transform.SetSiblingIndex(0);
        var healthBar = healthBarInstance.GetComponent<HealthBar>();
        healthBar.TargetGameObject = gameObject;

        DamagedSprite.gameObject.SetActive(false);
        FreezeSprite.gameObject.SetActive(false);
        CurrentHealth = MaxHealth;
        SetSpeed(Speed);
    }

    void SetSpeed(float speed)
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.down * speed;
    }

    public void Freeze(float time)
    {
        if (_freezed != null)
            StopCoroutine(_freezed);
        _freezed = StartCoroutine(ShowFreezed(time));
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        HomeArea homeArea;
        if (collision.gameObject.TryGetComponent<HomeArea>(out homeArea))
        {
            homeArea.GetDamage(Damage);
            Die();
        }
    }

    public virtual void GetDamage(int damage)
    {
        CurrentHealth -= damage;
        if (CurrentHealth <= 0)
            Die();
        else
            _dameged = StartCoroutine(ShowDamaged());
    }

    private IEnumerator ShowFreezed(float time)
    {
        if (_dameged != null)
        {
            StopCoroutine(_dameged);
            DamagedSprite.gameObject.SetActive(false);
            _dameged = null;
        }

        FreezeSprite.gameObject.SetActive(true);
        SetSpeed(0f);
        yield return new WaitForSeconds(time);
        FreezeSprite.gameObject.SetActive(false);
        SetSpeed(Speed);
        _freezed = null;
    }

    private IEnumerator ShowDamaged()
    {
        if (_freezed == null)
        {
            SetSpeed(0f);
            DamagedSprite.gameObject.SetActive(true);
            yield return new WaitForSeconds(WaitTimeWhenDamaged);
            SetSpeed(Speed);
            DamagedSprite.gameObject.SetActive(false);
        }
        _dameged = null;
    }

    private void Die()
    {
        Destroy(gameObject);
        Destryed?.Invoke(this);
    }
}
