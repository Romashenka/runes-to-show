﻿using UnityEngine;

namespace RunesProj
{

    ///Should add automatic calculation of forward and back bounds.
    public class GroundMarking : MonoBehaviour
    {
        
        [SerializeField]
        private float _mainGroundWidth = 20f;
        public float MainGroundWidth { get { return _mainGroundWidth; } }

        [SerializeField, Range(1, 6)]
        private int _roadsNumber = 3;
        public int RoadsNumber { get { return _roadsNumber; } }

        [SerializeField]
        private float _roadWidth = 6f;
        public float RoadWidth { get { return _roadWidth; } }

        [SerializeField]
        private Vector2 _spacing = Vector2.one * .5f;
        public Vector2 Spacing { get { return _spacing; } }

        [SerializeField]
        private Transform _roadPrefab = null;
        public Transform RoadPrefab { get { return _roadPrefab; } }

        [SerializeField]
        private Transform _mainGroundPrefab = null;
        public Transform MainGroundPrefab { get { return _mainGroundPrefab; } }

        [SerializeField, HideInInspector]
        private Transform _mainGround;
        public Transform MainGroud { get { return _mainGround; } }

        [SerializeField, HideInInspector]
        private Transform[] _roads;
        public Transform[] Roads { get { return _roads; } }

        [SerializeField]
        private float _forwardBound = 160f;
        public float ForwardBound { get { return _forwardBound; } }

        [SerializeField]
        private float _backBound = 20f;
        public float BackBound { get { return _backBound; } }

        public GroundGrid grid;

        private float[] xPositions;

        public float LastRightRoadXOffset
        { get { return _roadWidth * (_roadsNumber * .5f - .5f) + _spacing.x * ((_roadsNumber - 1f) * .5f); } }
        public float MaxXOffset
        { get { return _mainGroundWidth * .5f; } }

        public void SetGrid()
        {
            LevelSettings settings = Game.levelSettings;
            float enemyProb = settings.EnemyProbability;
            float wallProb = settings.WallProbability;
            int zCellsNumber = (int)(settings.LevelLength / _roadWidth);
            _spacing.y = settings.DistanceBetweenEnemies;
            grid = new GroundGrid(_roadsNumber, zCellsNumber, _roadWidth, _spacing, settings);
        }

        public Vector3 MoveToDirection(Vector3 currentPosition, float directionX)
        {
            int currentXIndex = grid.GridPosX(currentPosition.x) + (int)directionX;
            currentXIndex = (currentXIndex + _roadsNumber) % _roadsNumber;
            currentPosition.x = grid.XCoord(currentXIndex);
            return currentPosition;
            // float start = transform.position.x - LastRightRoadXOffset;
            // // Debug.Log(LastRightRoadXOffset);
            // float allRoadsWidth = (_roadWidth + _spacing.x) * RoadsNumber;
            // float xCurr = currentPosition.x - start;
            // xCurr += directionX * (_roadWidth + _spacing.x) + allRoadsWidth;
            // // Debug.Log(xCurr % allRoadsWidth / (_roadWidth + _spacing.x));
            // xCurr = xCurr % allRoadsWidth + start;
            // currentPosition.x = xCurr;
            // return currentPosition;
        }


        public float[] ValidXCoords()
        {
            float[] valid = new float[_roadsNumber];
            float lastRightX = LastRightRoadXOffset + transform.position.x;
            for (int i = 0; i < _roadsNumber; i++)
            {
                valid[i] = lastRightX - (_roadWidth + _spacing.x) * i;
            }
            return valid;
        }


#if UNITY_EDITOR
        public void SetNewRoadsAndGround(Transform[] roads, Transform mainGround)
        {
            _roads = roads;
            _mainGround = mainGround;
        }

        private void OnDrawGizmosSelected()
        {
            if (grid != null)
            {
                grid.DrawGrid(transform.position);
                grid.Draw();
            }
        }
#endif
    }
}