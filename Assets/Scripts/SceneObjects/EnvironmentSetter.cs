﻿using UnityEngine;
using System.Collections.Generic;


namespace RunesProj
{
    public class EnvironmentSetter : MonoBehaviour
    {
        public Transform player;
        public float step = 3f;

        private Transform[] envirObjects;
        private List<Transform> settedEnvir;
        private float spawnFieldMiddleXOffset;
        private float spawnFieldHalfWidth;
        private float forwardBound;
        private float backBound;
        void Start()
        {
            GroundMarking groundMarking = GetComponent<GroundMarking>();
            SetBounds(groundMarking);

            Game.levelSettings.GetEnvObjects(out envirObjects);
            int envObjNumber = Mathf.FloorToInt((forwardBound - backBound) / step); ;
            float startZ = player.position.z + backBound;
            settedEnvir = new List<Transform>(envObjNumber * 2);

            for (int i = 0; i < envObjNumber; i++)
            {
                SetObjectsLR(startZ + step * i);
            }
        }

        void Update()
        {
            Transform lastR = settedEnvir[0];
            float playerZ = player.position.z;
            if (lastR.position.z < playerZ + backBound)
            {
                SetObjectsLR(playerZ + forwardBound);
                Destroy(settedEnvir[0].gameObject);
                Destroy(settedEnvir[1].gameObject);
                settedEnvir.RemoveRange(0, 2);
            }
        }

        private void SetBounds(GroundMarking marking)
        {
            forwardBound = marking.ForwardBound;
            backBound = marking.BackBound;
            float innerBound = marking.LastRightRoadXOffset + marking.RoadWidth * .5f;
            spawnFieldMiddleXOffset = (marking.MaxXOffset - innerBound) * .5f + innerBound;
            spawnFieldHalfWidth = spawnFieldMiddleXOffset - innerBound;
        }

        private void SetObjectsLR(float zPos)
        {
            Vector3 myPos = transform.position;
            SetObject(myPos.x + spawnFieldMiddleXOffset, myPos.y, zPos);
            SetObject(myPos.x - spawnFieldMiddleXOffset, myPos.y, zPos);
        }

        private void SetObject(float centerXCoord, float yPos, float zPos)
        {
            float randX = Random.Range(-spawnFieldHalfWidth, spawnFieldHalfWidth);
            Vector3 pos = new Vector3(centerXCoord + randX, yPos, zPos);
            Transform next = Instantiate(RandEnvirObject(), pos, Quaternion.identity);
            settedEnvir.Add(next);
        }


        private Transform RandEnvirObject()
        {
            int rand = Random.Range(0, envirObjects.Length);
            return envirObjects[rand];
        }
    }
}