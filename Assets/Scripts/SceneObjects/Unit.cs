﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RunesProj
{
    public abstract class Unit : MonoBehaviour
    {
        public abstract void GetDamage(HitReceptor.HitInfo info);
    }
}
