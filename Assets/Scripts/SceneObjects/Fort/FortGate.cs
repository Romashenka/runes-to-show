﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RunesProj
{
    public class FortGate : Unit
    {
        public SpriteRenderer signSprite;
        public MeshRenderer meshRenderer;
        public Unit fort;
        public MotherOfEnemies mother;
        public int roadNumber;
        public Sign gateSign;

        private int signID;

        public Material activeMaterial;
        public Material closedMaterial;

        public Sprite activeSprite;

        public float cooldownTime = 3f;

        private bool opened;

        void Start()
        {
            TurnOn();
            // mother.enemyBorned += OnEnemySpawned;
        }

        void OnEnemySpawned(Enemy enemy, int roadNumber)
        {
            if (this.roadNumber == roadNumber && enemy is RuneEnemy)
            {
                RuneEnemy rE = enemy as RuneEnemy;
                signID = rE.sign.GetInstanceID();
                signSprite.sprite = rE.sprite;
                meshRenderer.sharedMaterial = rE.material;
            }
        }

        void TurnOn()
        {
            signSprite.sprite = activeSprite;
            meshRenderer.sharedMaterial = activeMaterial;
            signID = gateSign.GetInstanceID();
            opened = true;
        }

        void TurnOff()
        {
            signSprite.sprite = null;
            meshRenderer.sharedMaterial = closedMaterial;
            signID = -1;
            opened = false;
        }

        IEnumerator GoToCooldown()
        {
            TurnOff();
            yield return new WaitForSeconds(cooldownTime);
            TurnOn();
        }

        public override void GetDamage(HitReceptor.HitInfo info)
        {
            if (info.signID == signID)
            {
                fort.GetDamage(info);
                StartCoroutine(GoToCooldown());
            }
        }
    }
}