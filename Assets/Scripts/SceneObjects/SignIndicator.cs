﻿using UnityEngine;

namespace RunesProj
{
    public class SignIndicator : MonoBehaviour
    {
        private SpriteRenderer spriteRenderer;

        void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void SetSignSprite(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;
        }

    }
}