﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RunesProj
{
    public class RuneEnemy : Enemy
    {
        public SpriteRenderer spriteRenderer;
        public Sprite sprite { get { return spriteRenderer.sprite; } }

        public MeshRenderer meshRenderer;
        public Material material { get { return meshRenderer.sharedMaterial; } }
        public Sign sign;

        private int signID;

        public override void SetEnemy()
        {
            base.SetEnemy();
            signID = sign.GetInstanceID();
        }

        public override void GetDamage(HitReceptor.HitInfo hitInfo)
        {
            if (hitInfo.signID != 0 && signID != hitInfo.signID)
                return;
            base.GetDamage(hitInfo);
        }
    }
}