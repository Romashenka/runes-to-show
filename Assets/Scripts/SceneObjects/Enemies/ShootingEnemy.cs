﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RunesProj
{
    public class ShootingEnemy : Enemy
    {
        public Projectile projectilePrefab;
        private int phase = 0;
        private float shootPosZ;
        private float waitTime = 1f;
        public float offsetFromForwardBound = -3f;


        public override void SetEnemy()
        {
            base.SetEnemy();
            // shootPosZ = mother.marking.ForwardBound + offsetFromForwardBound;
        }

        void Update()
        {
            if (phase == 1 && transform.position.z > shootPosZ)
            {
                StartCoroutine(WaitAndShoot());
            }
        }

        IEnumerator WaitAndShoot()
        {
            phase++;
            direction = Vector3.zero;
            yield return new WaitForSeconds(waitTime);
            Projectile projectile = Instantiate(projectilePrefab, transform.position, transform.rotation);
            projectile.SetProjectile(null, Vector3.forward);
            // Debug.Log("POW!!!");
            direction = Vector3.forward;
        }

        public override void GetDamage(HitReceptor.HitInfo info)
        {
            if (phase == 0)
            {
                direction = Vector3.back;
                phase++;
            }
            else
                Die();

        }

        void OnDrawGizmos()
        {
            Vector3 pos = transform.position;
            pos.z = shootPosZ;
            Gizmos.DrawSphere(pos, 3f);
        }

    }
}