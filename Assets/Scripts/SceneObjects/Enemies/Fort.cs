﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RunesProj
{
    public class Fort : Unit, ISliderTarget
    {
        public float health = 1000f;
        private float healthCurrent;

        public float CurrentValue
        {
            get { return healthCurrent; }
            private set { healthCurrent = value; valueChanged(CurrentValue); }
        }
        public float MaxValue
        {
            get { return health; }
            private set { health = value; maxValueChanged(health); }
        }

        public event Action<float> valueChanged = (float f) => { };
        public event Action<float> maxValueChanged = (float f) => { };

        void Start()
        {
            CurrentValue = health;
        }

        public override void GetDamage(HitReceptor.HitInfo info)
        {
            CurrentValue -= 30f;
            if (healthCurrent <= 0f)
            {
                Game.WinReached();
            }
        }
    }
}