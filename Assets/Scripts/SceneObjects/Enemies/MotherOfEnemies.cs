﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace RunesProj
{
    public class MotherOfEnemies : MonoBehaviour
    {
        //////////
        private List<GameObject> spawned = new List<GameObject>(10);
        public Transform fort;
        public float spawnPeriod = 1f;
        ////////////
        public Enemy wallPrefab;
        public GroundMarking marking;
        public Transform player;
        public UnitInfo defaultEnemyStats;
        private Sign[] signs;
        private float enemiesDistance;

        private int rowsCount;
        private int previousSingIndex;
        private float[] validXCoords;

        private LevelSettings settings;

        private GroundGrid grid;

        private EnemiesSet enemiesSet;

        public event Action<Enemy, int> enemyBorned = (Enemy e, int road) => { };

        void Start()
        {
            validXCoords = marking.ValidXCoords();
            settings = Game.levelSettings;
            signs = settings.ValidEnemySigns;
            marking.SetGrid();
            grid = marking.grid;
            // Spawn();
            // SpawnRandom();
            enemiesSet = settings.AvaliableEnemies;
            SpawnInFortOnTime();
            // SpawnFromSet(settings.AvaliableEnemies);

            // enemiesDistance = settings.EnemiesDistanceMin;
            // int enCount = Mathf.FloorToInt(marking.ForwardBound / enemiesDistance);
            // for (int i = 1; i <= enCount; i++)
            // {
            //     SpawnEnemy();
            // }
        }

        // void Update()
        // {
        //     if (spawned.Count != 0)
        //     {
        //         TryToSpawnNextEnemy();
        //         TryToDestroyBackEnemy();
        //     }
        // }

        void TryToSpawnNextEnemy()
        {
            Transform forwardEnemy = spawned[spawned.Count - 1].transform;
            float forwardEnemyZ = forwardEnemy.position.z;
            float spawnZ = marking.ForwardBound - enemiesDistance;

            if ((forwardEnemyZ - player.position.z) < spawnZ)
            {
                SpawnEnemy();
            }
        }

        void TryToDestroyBackEnemy()
        {
            Transform backEnemy = spawned[0].transform;
            float backEnemyZ = backEnemy.position.z;

            if (backEnemyZ < player.position.z + marking.BackBound)
            {
                Destroy(backEnemy.gameObject);
                spawned.RemoveAt(0);
            }
        }

        private void SpawnInFortOnTime()
        {
            StartCoroutine(SpawnDelay());
            IEnumerator SpawnDelay()
            {
                Enemy randEn = enemiesSet.RandomEnemy;
                int randX = UnityEngine.Random.Range(0, grid.grid.GetLength(0));
                Vector3 pos = new Vector3(grid.XCoord(randX), 0f, fort.position.z);
                yield return new WaitForSeconds(spawnPeriod);
                Enemy enemy = Spawn<Enemy>(pos, randEn);
                enemy.SetEnemy();
                enemyBorned(enemy, randX);
                SpawnInFortOnTime();
            }
        }


        private void SpawnFromSet(EnemiesSet set)
        {
            int lenX = grid.grid.GetLength(0);
            EnemiesSet.Element[] ens = set.Enemies;
            List<EnemiesSet.Element> enemies = new List<EnemiesSet.Element>(ens.Length);
            enemies.AddRange(ens);

            int enemiesOnLevel = 0;
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i].count < 1)
                {
                    enemies.RemoveAt(i);
                    continue;
                }
                enemiesOnLevel += enemies[i].count;
            }

            Vector3 enemiesOffset = settings.DistanceToFirstEnemy * Vector3.forward;
            for (int i = 0; i < enemiesOnLevel; i++)
            {
                int posX = UnityEngine.Random.Range(0, lenX);
                int enemyIndex = UnityEngine.Random.Range(0, enemies.Count);

                Vector3 pos = grid.CellPos(posX, i) + enemiesOffset;
                Enemy e = Spawn<Enemy>(pos, enemies[enemyIndex].enemyPrefab);
                e.SetEnemy();
                // e.destroying += () => Game.Scores++;
                EnemiesSet.Element elem = enemies[enemyIndex];
                enemies[enemyIndex] = new EnemiesSet.Element(elem.enemyPrefab, elem.count - 1);
                if (enemies[enemyIndex].count == 0)
                    enemies.RemoveAt(enemyIndex);
            }
        }

        private void SpawnRandom()
        {
            int lenX = grid.grid.GetLength(0);
            int enemiesCount = settings.NumberOfEnemiesOnLevel;

            List<int> numbOfEnemiesOnRow = new List<int>();

            int enemiesOnLevel = settings.NumberOfEnemiesOnLevel;
            int enemiesMinInRow = settings.MinEnemyInRow;
            int enemiesMaxInRow = settings.MaxEnemyInRow;


            while (enemiesOnLevel > 0)
            {
                int maxEnemiesRand = Mathf.Min(enemiesMaxInRow, enemiesOnLevel);
                int rand = UnityEngine.Random.Range(enemiesMinInRow, maxEnemiesRand + 1);
                numbOfEnemiesOnRow.Add(rand);
                enemiesOnLevel -= rand;
            }

            Vector3 enemiesOffset = settings.DistanceToFirstEnemy * Vector3.forward;
            int rowsCount = numbOfEnemiesOnRow.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                int posX = UnityEngine.Random.Range(0, lenX + 1);

                for (int j = 0; j < numbOfEnemiesOnRow[i]; j++)
                {
                    posX++;
                    posX = posX % lenX;
                    Vector3 pos = grid.CellPos(posX, i) + enemiesOffset;
                    Sign s = GetRandom<Sign>(signs);
                    Enemy e = Spawn<Enemy>(pos, s.EnemyRand);
                    e.SetEnemy();
                    // e.destroying += () => Game.Scores++;
                }
            }
        }

        private void Spawn()
        {
            int lenX = grid.grid.GetLength(0);
            int lenZ = grid.grid.GetLength(1);
            for (int i = 0; i < lenX; i++)
            {
                for (int j = 0; j < lenZ; j++)
                {
                    switch (grid.grid[i, j])
                    {
                        case 2:
                            Vector3 pos = grid.CellPos(i, j);
                            Spawn<Enemy>(pos, wallPrefab);
                            break;
                        case 3:
                            pos = grid.CellPos(i, j);
                            Sign s = GetRandom<Sign>(signs);
                            Enemy e = Spawn<Enemy>(pos, s.EnemyRand);
                            e.SetEnemy();
                            break;
                            // default:
                    }
                }
            }
        }

        private T Spawn<T>(Vector3 pos, T obj) where T : MonoBehaviour
        {
            T spawnedObj = Instantiate(obj, pos, transform.rotation);
            spawned.Add(spawnedObj.gameObject);
            return spawnedObj;


            // rowsCount++;
            // for (int i = 0; i < numberOfWalls; i++)
            // {
            //     Vector3 position = transform.position + new Vector3(0f, 0f, enemiesDistance * rowsCount);
            //     int road = seedEnemyIndex % validXCoords.Length;
            //     position.x = validXCoords[road];
            //     Enemy wall = Instantiate(wallPrefab, position, transform.rotation);
            //     spawned.Add(wall);
            //     seedEnemyIndex++;
            // }

            // for (int i = 0; i < numberOfEnemies; i++)
            // {
            //     Sign randSign = GetRandom<Sign>(signs, ref previousSingIndex);
            //     Enemy randEnemyPrefab = randSign.EnemyRand;
            //     Vector3 position = transform.position + new Vector3(0f, 0f, enemiesDistance * rowsCount);
            //     int road = seedEnemyIndex % validXCoords.Length;
            //     // poses.Add(road);
            //     position.x = validXCoords[road];
            //     Enemy enemy = Instantiate(randEnemyPrefab, position, transform.rotation);
            //     enemy.SetEnemy(defaultEnemyStats, randSign.GetInstanceID());
            //     spawned.Add(enemy);
            //     seedEnemyIndex++;
            // }
        }

        private void SpawnEnemy()
        {

            int numberOfWalls = UnityEngine.Random.Range(settings.WallsMinInRow, settings.WallsMaxInRow + 1);
            int numberOfEnemies = UnityEngine.Random.Range(settings.MinEnemyInRow, settings.MaxEnemyInRow + 1);
            int over = (numberOfWalls + numberOfEnemies) - validXCoords.Length;
            numberOfEnemies -= Mathf.Max(0, over);
            int seedEnemyIndex = UnityEngine.Random.Range(0, validXCoords.Length);

            rowsCount++;
            for (int i = 0; i < numberOfWalls; i++)
            {
                Vector3 position = transform.position + new Vector3(0f, 0f, enemiesDistance * rowsCount);
                int road = seedEnemyIndex % validXCoords.Length;
                position.x = validXCoords[road];
                Enemy wall = Instantiate(wallPrefab, position, transform.rotation);
                spawned.Add(wall.gameObject);
                seedEnemyIndex++;
            }

            for (int i = 0; i < numberOfEnemies; i++)
            {
                Sign randSign = GetRandom<Sign>(signs, ref previousSingIndex);
                Enemy randEnemyPrefab = randSign.EnemyRand;
                Vector3 position = transform.position + new Vector3(0f, 0f, enemiesDistance * rowsCount);
                int road = seedEnemyIndex % validXCoords.Length;
                // poses.Add(road);
                position.x = validXCoords[road];
                Enemy enemy = Instantiate(randEnemyPrefab, position, transform.rotation);
                enemy.SetEnemy();
                spawned.Add(enemy.gameObject);
                seedEnemyIndex++;
            }
        }


        private Sign GetRandomSign()
        {
            int rand = UnityEngine.Random.Range(0, signs.Length);
            if (rand == previousSingIndex)
                rand = (rand + 1) % signs.Length;
            previousSingIndex = rand;
            return signs[rand];
        }

        private T GetRandom<T>(T[] arrey)
        {
            int rand = UnityEngine.Random.Range(0, arrey.Length);
            return arrey[rand];
        }

        private T GetRandom<T>(T[] arrey, ref int prevIndex)
        {
            int rand = UnityEngine.Random.Range(0, arrey.Length);
            if (rand == prevIndex)
                rand = (rand + 1) % arrey.Length;
            prevIndex = rand;
            return arrey[rand];
        }

    }
}