﻿using System;
using UnityEngine;


namespace RunesProj
{
    public class Enemy : Unit
    {
        public EnemyStats stats;
        public event Action destroying;
       
        private float speed;
        protected Vector3 direction = Vector3.back;

        public virtual void SetEnemy()
        {
            speed = stats.MoveSpeed;
        }

        void FixedUpdate()
        {
            transform.Translate(direction * Time.fixedDeltaTime * speed);
        }

        public override void GetDamage(HitReceptor.HitInfo hitInfo)
        {
            Die();
        }

        protected void Die()
        {
            destroying?.Invoke();
            gameObject.SetActive(false);
        }

        void OnTriggerEnter(Collider collider)
        {
            HitReceptor toCheck;
            if (collider.TryGetComponent<HitReceptor>(out toCheck))
            {
                toCheck.GetDamage(new HitReceptor.HitInfo());
                Die();
            }
        }



        // void CheckPlayer(Player playerToCheck)
        // {
        //     if (playerToCheck.CurrentSignID == SignID)
        //     {
        //         GetDamage(100f);
        //         // playerToCheck.stop = false;
        //     }
        //     else
        //     {
        //         if (Time.time > lastHitTime + hitPeriod)
        //         {
        //             playerToCheck.GetDamage(5f);
        //             lastHitTime = Time.time;
        //             // playerToCheck.stop = true;
        //         }
        //     }
        // }

    }
}