﻿using UnityEngine;

namespace RunesProj
{
    [CreateAssetMenu]
    public class EnemyStats : ScriptableObject
    {
        [SerializeField, SettingsField(1f, 100f, "Move Speed")]
        private float _moveSpeed = 5f;
        public float MoveSpeed { get { return _moveSpeed; } }

    }
}