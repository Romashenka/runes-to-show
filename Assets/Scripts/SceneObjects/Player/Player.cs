﻿using UnityEngine;
using RunesProj.DrawingInput;


namespace RunesProj
{
    public class Player : Unit
    {
        public Sign startSign;
        public PlayerStatus playerStats;
        public InputDir inputDir;
        public GroundMarking groundMarking;
        private Rigidbody myRigidbody;
        public int CurrentSignID { get; private set; }
        private float velocity = 5f;
        ///////
        public float damage = 100f;
        /////////
        private UnitsController unitsController;

        // public Projectile projectilePrefab;
        public Transform firePoint;

        private bool _stop;
        public bool Stop
        {
            get { return _stop; }
            set
            {
                _stop = value;
                if (value == true) myRigidbody.velocity = Vector3.zero;
            }
        }

        private float targetX = 0f;

        void Start()
        {
            playerStats.Reset();
            velocity = Game.levelSettings.MoveSpeed;
            myRigidbody = GetComponent<Rigidbody>();
            inputDir.inputFinished += AnswerInput;
            // unitsController = GetComponent<UnitsController>();
            // unitsController.SetImmediate(startSign);
            CurrentSignID = startSign.GetInstanceID();

            Time.timeScale = 1f;

            groundMarking?.SetGrid();
        }

        // void Update()
        // {
        //     if (!_stop)
        //     {
        //         myRigidbody.AddForce(Vector3.forward * velocity);
        //         Vector3 vel = myRigidbody.velocity;
        //         vel.z = Mathf.Clamp(vel.z, -velocity, velocity);
        //         myRigidbody.velocity = vel;
        //     }

        //     Vector3 target = transform.position;
        //     target.x = Mathf.Lerp(transform.position.x, targetX, velocity * .3f * Time.deltaTime);
        // }

        private void AnswerInput(Sign result, Vector2 pos)
        {
            if (result != null)
            {
                if (!result.IsControlSign)
                {
                    // CurrentSignID = result.GetInstanceID();
                    // unitsController.SetNextSign(result);
                    Shoot(result);
                }
                else
                    TryToMove(result.directionX);
            }
        }
        void Update()
        {
            // Debug.Log(groundMarking.grid.GridPosX(transform.position.x));
        }

        private void Shoot(Sign projSign)
        {
            Projectile projectile = Instantiate(projSign.ProjectilePrefab, firePoint.position, Quaternion.identity);
            projectile.SetProjectile(projSign, Vector3.forward);
        }

        // private void TryToDamage(Enemy enemy)
        // {
        //     if (enemy.SignID == CurrentSignID)
        //         enemy.GetDamage();
        // }

        private void OnTriggerEnter(Collider collider)
        {
            Enemy enemy = collider.GetComponent<Enemy>();
            // TryToDamage(enemy);
            if (enemy != null && enemy.gameObject.activeSelf)
            {
                Stop = true;
                enemy.destroying += () => Stop = false;
            }
        }

        // private void OnTriggerStay(Collider collider)
        // {
        //     Enemy enemy = collider.GetComponent<Enemy>();
        //     TryToDamage(enemy);
        // }

        private void OnTriggerExit(Collider collider)
        {
            Stop = false;
        }


        private void TryToMove(float directionX)
        {
            transform.position = groundMarking.MoveToDirection(transform.position, directionX);

        }

        public override void GetDamage(HitReceptor.HitInfo hitInfo)
        {
            playerStats.NumberOfLifes--;

            if (playerStats.NumberOfLifes <= 0f)
            {
                Die();
            }
        }
        void Die()
        {
            Game.GameOver();
        }
    }
}