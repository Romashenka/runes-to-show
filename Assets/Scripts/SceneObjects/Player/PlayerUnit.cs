﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnit : MonoBehaviour
{
    private Vector3 targetPos;
    private float velocity;

    void Update()
    {
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPos, velocity * Time.deltaTime);
    }

    public void SetTarget(Vector3 target, float swapTime)
    {
        targetPos = target;
        velocity = (targetPos - transform.localPosition).magnitude / swapTime;
    }

    public void JumpToTarget() { transform.localPosition = targetPos; }

}
