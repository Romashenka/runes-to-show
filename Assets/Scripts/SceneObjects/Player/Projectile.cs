﻿using System;
using UnityEngine;

namespace RunesProj
{
    public class Projectile : MonoBehaviour
    {
        public Material myMaterial;
        private int signID;
        private Vector3 direction;

        [SerializeField]
        private float damage = 100f;

        [SerializeField]
        private float speed = 5f;

        public event Action hit;

        public void SetProjectile(Sign sign, Vector3 direction)
        {
            this.direction = direction;
            signID = sign == null ? 0 : sign.GetInstanceID();
        }

        public void FixedUpdate()
        {
            transform.Translate(direction * Time.fixedDeltaTime * speed);
        }

        private void OnTriggerEnter(Collider collider)
        {
            HitReceptor receptor;
            if (collider.TryGetComponent<HitReceptor>(out receptor))
            {
                // if (enemy.SignID == signId)
                receptor.GetDamage(new HitReceptor.HitInfo(signID));
                hit?.Invoke();
                Destroy(gameObject);
            }
        }

    }
}