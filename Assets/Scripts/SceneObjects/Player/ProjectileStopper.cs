﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RunesProj
{
    public class ProjectileStopper : MonoBehaviour
    {
        void OnTriggerEnter(Collider collider)
        {
            Destroy(collider.gameObject);
        }
    }
}