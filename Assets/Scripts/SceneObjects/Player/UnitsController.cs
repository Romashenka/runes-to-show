﻿using UnityEngine;


namespace RunesProj
{
    public class UnitsController : MonoBehaviour
    {
        public float gridSize = 30f;
        public float swapTime = .5f;

        private BoxCollider commonCollider;

        public Vector3 gridRU
        {
            get
            {
                Vector3 oneXZ = Vector3.right + Vector3.forward;
                return oneXZ * gridSize / 2f - oneXZ * cellSize / 2f;
            }
        }

        private PlayerUnit[] units;

        private float cellSize { get { return gridSize / units.Length; } }

        public void Awake()
        {
            // cellSize = gridSize / units.Length;
            commonCollider = GetComponent<BoxCollider>();
            units = GetComponentsInChildren<PlayerUnit>();
        }

        public void SetImmediate(Sign sign)
        {
            SetNextSign(sign);
            for (int i = 0; i < units.Length; i++)
            {
                units[i].JumpToTarget();
            }
        }

        public void SetNextSign(Sign sign)
        {
            Vector2Int[] poss = sign.UnitsPositions;
            gridSize = sign.unitsArea;
            commonCollider.size = Vector3.one * sign.unitsArea;
            
            for (int i = 0; i < poss.Length; i++)
            {
                Vector3 localPos = GridPosition(poss[i]);
                units[i].SetTarget(localPos, swapTime);
            }
        }


        // public void Set(Sign sign)
        // {
        //     Vector2Int[] poss = sign.UnitsPositions;
        //     gridSize = sign.unitsArea;
        //     for (int i = 0; i < poss.Length; i++)
        //     {
        //         Vector3 localPos = GridPosition(poss[i]);
        //         units[i].SetTarget(localPos, swapTime);
        //     }
        // }

        private Vector3 GridPosition(Vector2Int pos)
        {
            Vector3 gridPos = new Vector3(pos.x, 0f, pos.y);
            return gridRU - gridPos * cellSize;
        }


        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, (Vector3.right + Vector3.forward) * gridSize);
        }
    }
}
