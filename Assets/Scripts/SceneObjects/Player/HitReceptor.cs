﻿using UnityEngine;

namespace RunesProj
{
    public class HitReceptor : MonoBehaviour
    {
        public Unit unit;

        public void GetDamage(HitInfo info) { unit.GetDamage(info); }

        public struct HitInfo
        {
            public int signID;

            public HitInfo(int signID)
            {
                this.signID = signID;
            }
        }
    }
}