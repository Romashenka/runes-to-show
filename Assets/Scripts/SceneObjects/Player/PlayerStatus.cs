﻿using System;
using UnityEngine;

namespace RunesProj
{
    [CreateAssetMenu(menuName = "PlayerStats")]
    public class PlayerStatus : ScriptableObject
    {
        [SerializeField, SettingsField(1, 10, "StartNumberOfLifes", true)]
        private int _startNumberOfLifes = 1;
        private int _currentNumberOfLifes;
        public Action<int> numberOfLifesChanged;
        public int NumberOfLifes
        {
            get { return _currentNumberOfLifes; }
            set
            {
                _currentNumberOfLifes = value;
                numberOfLifesChanged?.Invoke(_currentNumberOfLifes);
            }
        }


        public void Reset()
        {
            NumberOfLifes = _startNumberOfLifes;
        }

        public void SetStartNumberOfLifes(int val) { _startNumberOfLifes = val; }
    }
}