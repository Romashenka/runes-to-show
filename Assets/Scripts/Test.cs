﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public float dep;

    void OnDrawGizmos()
    {
        Camera mC = Camera.main;
        Vector3 v = new Vector3(.5f, 1f, dep);

        Gizmos.color = Color.red;
        v = mC.ViewportToWorldPoint(v);
        Gizmos.DrawLine(mC.transform.position, v);
        Gizmos.DrawWireSphere(v, 1f);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position + Vector3.forward * mC.farClipPlane, 5f);

    }

}
