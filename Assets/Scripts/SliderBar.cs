﻿using UnityEngine;
using UnityEngine.UI;

namespace RunesProj
{
    public class SliderBar : MonoBehaviour
    {
        public Slider slider;
        public GameObject targetObject;

        void Start()
        {
            ISliderTarget target;
            if (targetObject.TryGetComponent<ISliderTarget>(out target))
            {
                target.maxValueChanged += (float max) => slider.maxValue = max;
                target.valueChanged += (float val) => slider.value = val;

                slider.maxValue = target.MaxValue;
                slider.value = target.MaxValue;
            }
            else
            {
                Debug.LogError("No ISliderTarget compontnt on target object.");
            }
        }
    }
}