﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[CreateAssetMenu]
public class LevelData : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField] private List<LevelWave> _waves;
    [SerializeField] private float _timeBetweenWaves = 1f;

    [Space]
    [Space]
    [Header("Indicators")]
    [SerializeField] private float _summaryEnemiesWeight;
    public float LevelWeight => _summaryEnemiesWeight;

    public int WavesCount => _waves.Count;
    public float TimeBetweenWaves => _timeBetweenWaves;

    public LevelWave GetWave(int index) => _waves[index];


    public void OnAfterDeserialize() { }

    public void OnBeforeSerialize()
    {
        float summ = 0f;
        foreach (var wave in _waves)
        {
            summ += wave.WaveWeight;
        }
        _summaryEnemiesWeight = summ;
    }
}
