﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct MinMaxInt
{
    [SerializeField] private uint _min;
    [SerializeField] private uint _max;

    public int Min => (int)_min;
    public int Max => (int)_max;

    public MinMaxInt(uint min, uint max)
    {
        _min = min;
        _max = max;
    }
}
