﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneIndicator : MonoBehaviour
{
    [SerializeField] private GameObject _runeImage;
    [SerializeField] private float _fadeTime;

    private Coroutine _showCoroutine;

    public void ShowSign()
    {
        if (_showCoroutine != null)
            StopCoroutine(_showCoroutine);

        _showCoroutine = StartCoroutine(Show());
    }

    public void HideSign()
    {
        if (_showCoroutine != null)
            StopCoroutine(_showCoroutine);
        _runeImage.SetActive(false);
    }

    private IEnumerator Show()
    {
        _runeImage.SetActive(true);
        yield return new WaitForSeconds(_fadeTime);
        _runeImage.SetActive(false);
    }

}
