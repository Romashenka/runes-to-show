﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class EnemyData : ScriptableObject
{
    [SerializeField] private float _enemyWeight = 1f;
    public float EnemyWeight => _enemyWeight;

    [SerializeField] private Enemy _enemyPrefab;
    public Enemy EnemyPrefab => _enemyPrefab;
}
