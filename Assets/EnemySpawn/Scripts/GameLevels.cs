﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class GameLevels : ScriptableObject
{
    [SerializeField] private List<LevelData> _levelDatas;

    public LevelData GetLevel(int index)
    {
        return _levelDatas[index];
    }
}
