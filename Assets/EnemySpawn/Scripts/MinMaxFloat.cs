﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct MinMaxFloat
{
    [SerializeField] private float _min;
    [SerializeField] private float _max;

    public float Min => _min;
    public float Max => _max;

    public MinMaxFloat(float min, float max)
    {
        _min = min;
        _max = max;
    }
}
