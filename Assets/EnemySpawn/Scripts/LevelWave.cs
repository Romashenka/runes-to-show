﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class LevelWave
{
    //TODO: Check if min and max wave weight range is greater than largest waight for _waiveEnemies.
    [SerializeField] private EnemyData[] _waveEnemies;

    [SerializeField] private float _waveWeight = 20f;
    [SerializeField] private MinMaxFloat _groupWeight;
    [SerializeField] private MinMaxFloat _waveSpawnPeriod;

    public float WaveWeight => _waveWeight;
    public MinMaxFloat GroupWeight => _groupWeight;
    public MinMaxFloat WaveSpawnPeriod => _waveSpawnPeriod;

    public EnemiesGroup NextEnemiesGroup(float currentWaveWeight)
    {
        var randomGroupWeight = Random.Range(_groupWeight.Min, _groupWeight.Max);
        var randomEnemy = Utility.RandomElement(_waveEnemies);
        randomGroupWeight = Mathf.Clamp(randomGroupWeight, randomEnemy.EnemyWeight, randomGroupWeight);

        return new EnemiesGroup(randomEnemy, (int)(randomGroupWeight / randomEnemy.EnemyWeight));

    }

    public float NextGroupTime()
    {
        return Random.Range(_waveSpawnPeriod.Min, _waveSpawnPeriod.Max);
    }
}

public struct EnemiesGroup
{
    public readonly EnemyData EnemyData;
    public readonly int GroupSize;

    public EnemiesGroup(EnemyData enemyData, int groupSize)
    {
        EnemyData = enemyData;
        GroupSize = groupSize;
    }
}
