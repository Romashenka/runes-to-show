﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct MinMaxRange<T>
{
    public T Min { get; private set; }
    public T Max { get; private set; }

    public MinMaxRange(T min, T max)
    {
        Min = min;
        Max = max;
    }
}
